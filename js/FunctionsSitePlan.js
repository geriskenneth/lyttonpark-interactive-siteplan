/* All shared functions for the siteplan go in here */

function buildSiteplanLayout(){
     // For performance, Make sure to remove old elements when redrawing
    svg.selectAll('text').remove();
    svg.selectAll('circle').remove();
    
    // Loop for each session, show on the siteplan and add house if sold
    for (var i in g_colLotSessions) {        
        if (g_colLotSessions.hasOwnProperty(i)) {            
            
            var intLotId = g_colLotSessions[i].id_lot;
            // console.log(intLotId);
            var objLot = g_colLotTable[intLotId];    
            highlightLot(objLot, STATUS.ID_SOLD);
        }
    }
    
    for (var i in g_colLotSessions) {        
        if (g_colLotSessions.hasOwnProperty(i)) {            
            
            var s = g_colLotSessions[i];            
            var intLotId = s.id_lot;
            var objLot = g_colLotTable[intLotId];    
            var text = 'Placeholder A';
            addText(objLot, text, intLotId);            
        }
    }  
}

function buildSessionOverlay(){  
    
    // For performance, Make sure to remove old elements when redrawing
    svg.selectAll('text').remove();
    svg.selectAll('circle').remove();
    
    // Loop for each session, show on the siteplan and add house if sold
    for (var i in g_colLotSessions) {        
        if (g_colLotSessions.hasOwnProperty(i)) {            
            
            var intLotId = g_colLotSessions[i].id_lot;
            var objLot = g_colLotTable[intLotId];    
            var intStatusId = parseInt(g_colLotSessions[i].id_status); 
            highlightLot(objLot, intStatusId);
        }
    }
    
    for (var i in g_colLotSessions) {        
        if (g_colLotSessions.hasOwnProperty(i)) {            
            
            var s = g_colLotSessions[i];
            
            var intLotId = s.id_lot;
            var objLot = g_colLotTable[intLotId];    
            var intStatusId = parseInt(s.id_status); 
            if(intStatusId == STATUS.ID_SOLD || intStatusId == STATUS.ID_PENDING){
                // console.log( g_colModels[g_colLots[intLotId].size]);
                var n = g_colModels[g_colLots[intLotId].size][s.id_model][s.elevation];
                var text = n.model + ' ' + n.elevation;
                addText(objLot, text, intLotId);
            }
        }
    }   
}

function highlightLot(objLot, intStatusId){    
    // For each status, do something on the siteplan
    // For now it just colors a lot    

    objLot.style.strokeWidth = '0px';
    
    switch(intStatusId){
        case STATUS.ID_AVAIL:
            objLot.style.opacity = 0.0;
            break;
        case STATUS.ID_UNAVAILABLE:
            objLot.style.fill = 'grey';
            objLot.style.stroke = 'black';
            objLot.style.strokeWidth = '0.5px';
            objLot.style.opacity = 1.0;
            objLot.setAttribute("fill-opacity", "1.0");
            break;
        case STATUS.ID_PENDING:
            addDot(objLot, '#ffc107');
            objLot.style.fill = 'green';
            objLot.style.stroke = 'black';
            objLot.style.opacity = 1;
            objLot.setAttribute("fill-opacity", "1.0");
            break;
            
        case STATUS.ID_SOLD:            
            //addHouse(objLot);  
            addDot(objLot, 'red');
            objLot.style.fill = 'green';
            objLot.style.stroke = 'black';  
            objLot.style.strokeWidth = '0.5px';
            objLot.style.opacity = 1;
            objLot.setAttribute("fill-opacity", "1.0");
            break;
    }  
}

function addMask(objLot){
    objLot.style.fill = 'darkgrey';
    objLot.style.stroke = 'darkgrey';
    objLot.style.strokeWidth = '3px';
    objLot.style.opacity = '0.8';
}

function addDot(objLot, color){
    var pathBounds = objLot.getBBox();
    
    var svgns = "http://www.w3.org/2000/svg";
    var shape = document.createElementNS(svgns, "circle");

    // TODO store these in firebase and apply
    // scale factor
    
    var s = 1;    
    
    var lotId = objLot.id.split('_')[1];
    
    if(g_colLots[lotId].scale){
        s = parseFloat(g_colLots[lotId].scale);
        // console.log(s);
    }
    
    
    var lotW = pathBounds.width;
    var lotH = pathBounds.height;
    
    var radius = Math.min(lotW/2, lotH/2) * 0.5 * s;
    
    
    
    shape.setAttributeNS(null, "cx", pathBounds.x + lotW/2);
    shape.setAttributeNS(null, "cy", pathBounds.y + lotH/2);
    shape.setAttributeNS(null, "r",  radius);
    shape.setAttributeNS(null, "fill", color); 
    shape.setAttributeNS(null, "opacity", "0.8"); 
    
    //shape.setAttribute('transform', "translate(" + x +"," + y + ") scale(" + scaleX +") rotate(" + r + ")");
    g.node().appendChild(shape); 
}

/* Not needed for newseaton project */
function addHouse(objLot){
    var pathBounds = objLot.getBBox();
    
    var house = g_IconSoldHouse.querySelector('#Soldhouse').cloneNode(true);
    var houseW = 40;
    var houseH = 48;
    var lotW = pathBounds.width;
    var lotH = pathBounds.height;
    
    var scaleX = (pathBounds.width / houseW)*0.5;
    
    if(lotW > lotH){
        scaleX = (pathBounds.height / houseW)*0.5;
    }

    var x = pathBounds.x + (lotW/2) - (houseW*scaleX)/2;
    var y = pathBounds.y + (lotH/2) - (houseW*scaleX)/2;

    // TODO store these in firebase and apply
    // Stored rotate, scale and translate factor
    var r = 0;
    var s = 0;
    var t = 0;
    
    house.setAttribute('transform', "translate(" + x +"," + y + ") scale(" + scaleX +") rotate(" + r + ")");
    g.node().appendChild(house);  
}

function addText(objLot, text, lotId){
    
    var pathBounds = objLot.getBBox();
    var x = pathBounds.x + pathBounds.width/2;
    var y = pathBounds.y + pathBounds.height/2;
    
    // If horizontal lot, rotate 90 degrees
    var r = 0;
    if(pathBounds.height > pathBounds.width){
        r = 90;
    }

    if(g_colLots[lotId].textrotation){
        r += parseInt(g_colLots[lotId].textrotation);
    }
    
    g.append('text')                
    .attr("x", x)
    .attr("y", y)                
    .attr("font-family", "sans-serif")
    .attr("font-size", "5px")
    .attr("fill", "white")
    .attr("dominant-baseline" ,"central")
    .attr("text-anchor" ,"middle")
    .attr("transform", 'rotate(' + r + ',' + x + ',' + y + ')')
    .text(text);
}


function setupSitePlan(){
    var objPaths = g_svg_siteplan.find("path");
    // Keep track of existing lot paths in a hashmap
    var hashMapPaths = {};
    
    // Loop for each lot, write to database 
    for(var i = 0; i < objPaths.length; i++){
        if(objPaths[i].id){
            console.log(objPaths[i].id);
            // Create lot            
            var splitUpId = objPaths[i].id.split('_')[1];
            // Split up id Lot_50-Phase1 to Lot and 50-Phase1 
            
            // Add entry in the hashmap
            hashMapPaths[splitUpId] = true;
            
            // Currently setting area size to 36 for each lot since there is no data to import this from
            // Currently setting premiums to 1 since there is no data to import from            
            var objLot = new Lot(splitUpId, 36, 1, "PWOB", "18-Dec-20");
            saveLot(objLot);
            console.log(objLot);
            // Set everything to available in lotsessions
            var intStatusId = STATUS.ID_AVAIL;
            var intClientId = 0;
            var intModelId = 0;
            var strElevationCode = "0";
            var strCreatedDate = new Date().YYYYMMDDHHMMSS();

            var objLotSession = new LotSession(splitUpId, intStatusId, intClientId, intModelId, strElevationCode, strCreatedDate);
            saveLotSession(objLotSession);
            console.log(objLotSession);
        }  
    }  
    
    // Loop for each lot, ranges to database 
     // Set everything in range to number -2 and +2
    // Can't know what lot is across the street, set these manually
    // Always check each lot manually because of exceptions
    // Set blocked to nothing at start, only when lots are sold
    var hashmapEdges = [];
    for(var i = 0; i < objPaths.length; i++){
        if(objPaths[i].id){           
            // Split up id 50-Phase1 to 50 and Phase1
            var strLotId = objPaths[i].id.split('_')[1];
            var ids = strLotId.split('-');
            
            // Singe Rule of having nearby exact same model
            var StartNr = parseInt(ids[0])-2;
            var LotNr = parseInt(ids[0]);  
            
             var objLotconnection = new LotConnection(strLotId, strLotId, 1, [], []);
            saveLotConnection(objLotconnection);
            console.log(objLotconnection);
            
            // for(var j = StartNr; j <= LotNr+2; j++){
                
            //     var strConnectedId = j + '-' + ids[1];
            //     if(j !== LotNr && hashMapPaths[strConnectedId]){                    
                    
            //         var objLotConnectionKey = strLotId + strConnectedId;
                    
            //         if(!hashmapEdges[objLotConnectionKey]){
                        
            //             // Rule not equal m and e applied
            //             // First time, assuming no lots are bought set blocked to empty
            //             var intRuleNumber = RULES.ID_NEQ_M_E;
            //             var objLotconnection = new LotConnection(strLotId, strConnectedId, intRuleNumber, [], []);
            //             saveLotConnection(objLotconnection);                        
                        
            //             hashmapEdges[objLotConnectionKey] = true;
            //         }
            //     }
            // }
            
           
           
        }
    }
    
    // Create the status database with all the rules
    // TODO: group rules
    
    saveRule(RULES.ID_NEQ_M_E, RULES.NEQ_M_E);
    saveRule(RULES.ID_MEQ_B, RULES.MEQ_B);
    saveRule(RULES.ID_NEQ_M, RULES.NEQ_M);
    saveRule(RULES.ID_NEQ_E, RULES.NEQ_E); 
    
    // Create the status database with 4 statuses
    
    saveStatus(STATUS.ID_AVAIL, STATUS.AVAIL);
    saveStatus(STATUS.ID_UNAVAILABLE, STATUS.UNAVAILABLE);
    saveStatus(STATUS.ID_PENDING, STATUS.PENDING);
    saveStatus(STATUS.ID_SOLD, STATUS.SOLD);   
    
    
    // Create the models, this case imported from a csv    
    $.getJSON("productsLyttonpark.json", function(json) {      
        for(var i = 0; i < json.length; i++){
            var obj = json[i];
            var objModel = new Model(obj.code, obj.model, obj.elevation, obj.size, obj.opt, obj.area, obj.bedrooms, obj.bathrooms, obj.price);
            saveModel(objModel); 
        }  
    });
    
    // Create the clients
    var objDummyClient = new Client('Kenneth', 'Geris', '1063 Eglinton Avenue', "TBD", "geriskenneth@gmail.com");
    saveClient(objDummyClient);
    
}

