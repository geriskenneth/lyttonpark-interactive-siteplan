
var g_colModels;
var g_colLots;
var g_colLotSessions;

// Table containing the currently available lots based on what's selected
// Adapts each time the user selects product line / model / elevation options
var g_colAvailable = {};
var g_blnSiteplanInitialized = false;

var g_intSelectedProductLine;
var g_intSelectedModel;
var g_intSelectedModelOptions;
var g_intSelectedLot;

var g_svgSelectedLot;

var g_dataTableLots;
var g_dataTableModels;

var compare1, compare2;

/* This stores all the colors that will be used in the theme corresping to each product line */
var PLCOLORS = {
    20: '#bf2abc',
    30: '#4e8f85',
    31: '#4e8f85',
    40: '#916f89',
    45: '#9aa542',
};

var PLCOLORS_INFO = {
    20: '#f5f5f5',
    30: '#f5f5f5',
    31: '#f5f5f5',
    40: '#f5f5f5',
    45: '#f5f5f5',
};

// Elements that will be reused multiple times, load once and add transformed element to desired element
var ICON_VIEW_URL = "img/Icons/button_go.svg";
var ICON_BATHUB_URL = "img/Icons/bathtub.svg";
var ICON_COIN_URL = "img/Icons/coin.svg";
var ICON_AREA_URL = "img/Icons/area.svg";
var ICON_BED_URL = "img/Icons/bed.svg";
var ICON_DIAMOND_URL = "img/Icons/diamond.svg";
var ICON_HOMESITES_URL = "img/Icons/placeholder.svg";
var ICON_SOLDHOUSE_URL = "img/Icons/soldhouse.svg";

// References to the elements that will be changed in color
var ICON_VIEW_CHANGE = "Inner_circle";
var ICON_DIAMOND_CHANGE = "Diamond";
var ICON_AREA_CHANGE = 'Area';
var ICON_PRICE_CHANGE = 'Price';
var ICON_BATH_CHANGE = 'Bath';
var ICON_BED_CHANGE = 'Bed';
var ICON_HOMESITES_CHANGE = "Homesites";

var g_IconView, g_IconBath, g_IconCoin, g_IconArea, g_IconBed, g_IconDiamond, g_IconHomesites, g_IconSoldHouse;


var minAvailablePrice = 9999999, maxAvailablePrice = 0, minAvailableArea = 999999, maxAvailableArea = 0, minAvailableBedroom = 999999, maxAvailableBedroom = 0,
minAvailableBathroom = 999999, maxAvailableBathroom = 0;
var intSearchMinPrice, intSearchMaxPrice, intSearchMinArea, intSearchMaxArea;
var pl, bedrooms, bathrooms;

/* navigation vars */
var activeFooterSvg;
var activeScreen;
var scrollPosition;
var g_lockedAccordion = false;
var blnOptToggle;

function applySvgStyle(tab) {
    activeFooterSvg = tab.querySelector('object').contentDocument.querySelector('svg');
    activeFooterSvg.style.fill = 'black';
}

function removeSvgStyle() {
    if (activeFooterSvg) {
        activeFooterSvg.style.fill = '#000';
    } else {
        // if no active footer svg, by default just remove from design
        $('#icon_house').get(0).contentDocument.querySelector('svg').style.fill = '#000';
    }
}

function backFunction() {

    var activeId = activeScreen.id;
    var strActiveMenuItem = $('a.activeMenu').get(0).id;

    switch (strActiveMenuItem) {

        case "linkscreen1":
            switch (activeId) {
                case "linkscreen2":
                    $('#linkscreen1').trigger("click");
                    break;
                case "linkscreen3":
                    $('#linkscreen2').trigger("click");
                    break;
                case "linkscreen4":
                    $('#linkscreen3').trigger("click");
                    break;
            }
            break;
        case "linksearch":
            switch (activeId) {
                case "linkscreen3":
                    $('#linksearch').trigger("click");
                    break;
                case "linkscreen4":
                    $('#linkscreen3').trigger("click");
                    break;                      
            }
            break;
        case "linkscreen4":
            switch (activeId) {
                case "linkscreen3":
                    $('#linkscreen4').trigger("click");
                    break;
            }
            break;
        case "linkcompare":
            switch (activeId) {
                case "linkscreen4":
                    $('#linkcompare').trigger("click");
                    break;
                case "linkscreen3":
                    $('#linkcompare').trigger("click");
                    break;
            }
            break;
    }
}

function refreshSliders() {
    $('#slider_area').slider('refresh');
    $('#slider_price').slider('refresh');

    $('#filters_slider_area_min').html(addCommas(intSearchMinArea));
    $('#filters_slider_area_max').html(addCommas(intSearchMaxArea));
    $('#filters_slider_price_min').html(addCommas(intSearchMinPrice));
    $('#filters_slider_price_max').html(addCommas(intSearchMaxPrice));
}

/* Filters out from the existing search view, what doesn't fit the parameters */
/* Needsto know if it was a slider action or not*/
function filterSearch(blnSlider) {
    if (!blnSlider) {
        intSearchMinArea = minAvailableArea;
        intSearchMaxArea = maxAvailableArea;
        intSearchMinPrice = minAvailablePrice;
        intSearchMaxPrice = maxAvailablePrice;
    }

    $('.search_group_pl .activeButton').removeClass('activeButton');
    $('.search_group_bedrooms .activeButton').removeClass('activeButton');
    $('.search_group_bathrooms .activeButton').removeClass('activeButton');

    $('.search_group_pl div[data-val="' + pl + '"]').addClass('activeButton');
    $('.search_group_bedrooms div[data-val="' + bedrooms + '"]').addClass('activeButton');
    $('.search_group_bathrooms div[data-val="' + bathrooms + '"]').addClass('activeButton');

    var lotTypes;
    // if all or 50+ selected, set case
    if (pl === 'all') {
        lotTypes = [20, 30, 31, 40, 45];
    } else {
        lotTypes = [pl];
    }

    var colMatching = [];
    var newSearchMinArea = 9999999;
    var newSearchMaxArea = 0;
    var newSearchMinPrice = 9999999;
    var newSearchMaxPrice = 0;


    for (var i in lotTypes) {
        var objPL = g_colModels[lotTypes[i]];
        
        for (var k in objPL) {
            
            for (var l in objPL[k]) {
                var intMPrice = parseInt(objPL[k][l].price);

                var intMArea = parseInt(objPL[k][l].size);
                var intMBedrooms = parseInt(objPL[k][l].bedrooms);
                var strMBathrooms = parseFloat(objPL[k][l].bathrooms);

                var intOBedrooms = -1;
                var strOBathrooms = -1;

                var intOSize = 0;
                var intOPrice = 0;
                if (objPL[k][l].opt && objPL[k][l].opt.size) {
                    intOSize = parseInt(objPL[k][l].opt.size);
                }
                if (objPL[k][l].opt && objPL[k][l].opt.price) {
                    intOPrice = parseInt(objPL[k][l].opt.price);
                }

                if (objPL[k][l].opt && objPL[k][l].opt.bedrooms) {
                    intOBedrooms = parseInt(objPL[k][l].opt.bedrooms);
                }

                if (objPL[k][l].opt && objPL[k][l].opt.bathrooms) {
                    strOBathrooms = parseFloat(objPL[k][l].opt.bathrooms);
                }

                var blnBed = true
                        , blnBath = true
                        , blnPrice = false
                        , blnArea = false;

                if (bedrooms !== 'all') {
                    var intBeds = parseInt(bedrooms);


                    if (intBeds !== 5 && intBeds !== intMBedrooms) {

                        blnBed = false;

                        if (Number.isInteger(intOBedrooms) && intBeds === intOBedrooms) {
                            blnBed = true;
                        }
                    }

                    if (intBeds === 5 && intMBedrooms < intBeds) {

                        blnBed = false;

                        if (Number.isInteger(intOBedrooms) && intOBedrooms >= intBeds) {
                            blnBed = true;
                        }
                    }
                }

                if (bathrooms !== 'all') {
                    var floatBaths = parseFloat(bathrooms);

                    if (floatBaths !== 4.5 && floatBaths !== strMBathrooms) {
                        blnBath = false;

                        if (!isNaN(parseFloat(strOBathrooms)) && floatBaths === strOBathrooms) {
                            blnBath = true;
                        }
                    }

                    if (floatBaths === 4.5 && strMBathrooms < floatBaths) {
                        blnBath = false;

                        if (!isNaN(parseFloat(strOBathrooms)) && strOBathrooms >= floatBaths) {
                            blnBath = true;
                        }
                    }
                }

                if (intMPrice >= intSearchMinPrice && intMPrice <= intSearchMaxPrice
                        || intOPrice >= intSearchMinPrice && intOPrice <= intSearchMaxPrice) {
                    blnPrice = true;
                }

                if (intMArea >= intSearchMinArea && intMArea <= intSearchMaxArea
                        || intOSize >= intSearchMinArea && intOSize <= intSearchMaxArea) {
                    blnArea = true;
                }
                // when all are matching, add to found list
                if (blnBed && blnBath && blnPrice && blnArea) {
                    colMatching.push('search_' + k + '-' + l);
                    if (g_colAvailable[k][l + "_count"] > 0 && !blnSlider) {

                        var x = k.split('-')[0];


                        var intSize = parseInt(g_colModels[x][k][l].size);
                        var intPrice = parseInt(g_colModels[x][k][l].price);

                        if (intSize > newSearchMaxArea) {
                            newSearchMaxArea = intSize;
                        }

                        if (intSize < newSearchMinArea) {
                            newSearchMinArea = intSize;
                        }

                        if (intPrice > newSearchMaxPrice) {
                            newSearchMaxPrice = intPrice;
                        }

                        if (intPrice < newSearchMinPrice) {
                            newSearchMinPrice = intPrice;
                        }

                        if (g_colModels[x][k][l].opt) {

                            // If this model has options or alternatives and they have overwriting values
                            // Include the overwriting values of these into the ranges
                            if (g_colModels[x][k][l].opt.size) {
                                var intOptSize = parseInt(g_colModels[x][k][l].opt.size);

                                if (intOptSize > newSearchMaxArea) {
                                    newSearchMaxArea = intOptSize;
                                }

                                if (intOptSize < newSearchMinArea) {
                                    newSearchMinArea = intOptSize;
                                }
                            }

                            if (g_colModels[x][k][l].opt.price) {
                                var intOptPrice = parseInt(g_colModels[x][k][l].opt.price);
                                if (intOptPrice > newSearchMaxPrice) {
                                    newSearchMaxPrice = intOptPrice;
                                }

                                if (intOptPrice < newSearchMinPrice) {
                                    newSearchMinPrice = intOptPrice;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    $('#screensearch .screen2_template_container').hide();
    

    var results = 0;
    for (var i in colMatching) {
        if ($('div[id="' + colMatching[i] + '"]').length > 0) {
            results++;
        }
        $('div[id="' + colMatching[i] + '"]').fadeIn(500);
    }

    $('#filters_count').html(results + " results");

    if (!blnSlider) {

        if (newSearchMinArea === 9999999) {
            newSearchMinArea = 0;
        }
        if (newSearchMinPrice === 9999999) {
            newSearchMinPrice = 0;
        }

        // console.log(newSearchMinPrice, newSearchMaxPrice, newSearchMinArea, newSearchMaxArea);


        $('#slider_area').data('slider-min', newSearchMinArea);
        $('#slider_area').data('slider-max', newSearchMaxArea);
        $('#slider_area').slider({
            min: newSearchMinArea,
            max: newSearchMaxArea,
            value: [newSearchMinArea, newSearchMaxArea]
        });

        $('#slider_price').data('slider-min', newSearchMinArea);
        $('#slider_price').data('slider-max', newSearchMaxArea);
        $('#slider_price').slider({
            min: newSearchMinPrice,
            max: newSearchMaxPrice,
            value: [newSearchMinPrice, newSearchMaxPrice]
        });

        $('#slider_area').slider('refresh');
        $('#slider_price').slider('refresh');

        $('#filters_slider_area_min').html(addCommas(newSearchMinArea));
        $('#filters_slider_area_max').html(addCommas(newSearchMaxArea));
        $('#filters_slider_price_min').html(addCommas(newSearchMinPrice));
        $('#filters_slider_price_max').html(addCommas(newSearchMaxPrice));

    }
}

function initFilteredModels() {
    var lotTypes = [20, 30, 31, 40, 45];
    g_colAvailable = {};

    for (var i in lotTypes) {

        var objPL = g_colModels[lotTypes[i]];

        for (var k in objPL) {

            g_colAvailable[k] = {};

            if (objPL.hasOwnProperty(k)) {
                for (var l in objPL[k]) {
                    if (objPL[k].hasOwnProperty(l)) {


                        var intLotsAvailable = 0;
                        var availableLots = findAvailableLotsAC(lotTypes[i], k, l);

                        // Initialize this once at startup, on session change then make sure this refreshes
                        for (var p in availableLots) {
                            if (availableLots.hasOwnProperty(p) && parseInt(availableLots[p]) !== 0) {
                                intLotsAvailable++;
                                g_colAvailable[k][l] = availableLots;
                            }
                        }

                        g_colAvailable[k][l + '_count'] = intLotsAvailable;
                    }
                }
            }
        }
    }
}




function initSliders() {
    $("#slider_price").slider({});
    $("#slider_area").slider({});

    $("#slider_price").on("slide", function (e) {
        intSearchMinPrice = e.value[0];
        intSearchMaxPrice = e.value[1];
        $('#filters_slider_price_min').html(addCommas(e.value[0]));
        $('#filters_slider_price_max').html(addCommas(e.value[1]));
    });

    $('#slider_price').on('slideStop', function (e) {
        intSearchMinPrice = e.value[0];
        intSearchMaxPrice = e.value[1];
        $('#filters_slider_price_min').html(addCommas(e.value[0]));
        $('#filters_slider_price_max').html(addCommas(e.value[1]));
        filterSearch(true);
    });

    $("#slider_area").on("slide", function (e) {
        intSearchMinArea = e.value[0];
        intSearchMaxArea = e.value[1];
        $('#filters_slider_area_min').html(addCommas(e.value[0]));
        $('#filters_slider_area_max').html(addCommas(e.value[1]));
    });

    $('#slider_area').on('slideStop', function (e) {
        intSearchMinArea = e.value[0];
        intSearchMaxArea = e.value[1];
        $('#filters_slider_area_min').html(addCommas(e.value[0]));
        $('#filters_slider_area_max').html(addCommas(e.value[1]));
        filterSearch(true);
    });

    $("#zoomslider").slider({
        min: 1,
        max: 5,
        value: 1,
        orientation: 'vertical',
        reversed: true
    });

    $("#zoomslider").on("slide", function (e) {
        zoomManually(e.value);
    });

    $('#div_zslider .slider').on("slideStop", function (e) {
        scale = e.value
        svg.transition().duration(500).call(zoom.scaleTo, e.value);
    });

    $('#btn_siteplanzoomin').on("touchstart", function () {
        var newScale = Math.min(5, scale + 1);
        scale = newScale;
        svg.transition().duration(500).call(zoom.scaleTo, newScale);
        if(scale > 3){
            $('.keymap').fadeOut()
        }else{
          $('.keymap').fadeIn()
        }
    });

    $('#btn_siteplanzoomout').on("touchstart", function () {
        var newScale = Math.max(1, scale - 1);
        scale = newScale;
        svg.transition().duration(500).call(zoom.scaleTo, newScale);
        if(scale > 3){
            $('.keymap').fadeOut()
        }else{
          $('.keymap').fadeIn()
        }
    });

    $('#btn_siteplanreset').on("touchstart", function () {
        scale = 1;
        svg.transition().duration(1000).call(zoom.scaleTo, scale);
        if(scale > 3){
            $('.keymap').fadeOut()
        }else{
          $('.keymap').fadeIn()
        }
    });
}

function switchTab(tab) {
    activeScreen = tab;
    $(tab).tab('show');
    $('a.activeMenu').removeClass('activeMenu');
    $(tab).addClass('activeMenu');
    removeSvgStyle();
    applySvgStyle(tab);
    $('#screen4').removeClass('activeMenu');
    $('#screen4').removeClass('show');
    $('#screen4').attr('aria-expanded', "false");
}

function resetSitePlan() {
    $('#linkidle').trigger('click');
    $('#filters_btn_clear').trigger('click');
    $("#compare_container_clear").trigger('click');
    g_intSelectedLot = null;
    reset();
}

function initMenuItems() {
    $('.container_footer_back').click(function (e) {
        $(this).css('background-color', '#231f20');

        setTimeout(function () {
            $('.container_footer_back').css('background-color', '#231f20');
        }, 250);

        backFunction();
    });

    var timeoutId = 0;

    $('#logo_community').on('mousedown touchstart', function () {
        timeoutId = setTimeout(resetSitePlan, 2000);
    }).on('mouseup mouseleave touchend', function () {
        clearTimeout(timeoutId);
    });

    $('#screen2_content').on('scroll', function () {
        var position = $(this).scrollLeft();

        if (position > 15) {
            $('.screen2_leftarrow').show();
        } else {
            $('.screen2_leftarrow').hide();
        }

        if (position < $(this)[0].scrollWidth - $(this).width() - 15) {
            $('.screen2_rightarrow').show();
        } else {
            $('.screen2_rightarrow').hide();
        }
    });

    $('.screen2_rightarrow').on('click', function () {
        var scrollAmount = $('#screen2_content').scrollLeft();
        var scrollPos = scrollAmount + 1575;

        $('#screen2_content').animate({scrollLeft: scrollPos}, 500);
    });

    $('.screen2_leftarrow').on('click', function () {
        var scrollAmount = $('#screen2_content').scrollLeft();
        var scrolloffset = scrollAmount % 1575;
        var scrollPos = scrollAmount - scrolloffset;

        if (scrolloffset === 0) {
            scrollPos -= 1575;
        }

        $('#screen2_content').animate({scrollLeft: scrollPos}, 500);
    });

    $('#screen2_menu').on('click', "div", function () {

        var m = $(this).attr('data-model');
        var scrollPos = $('#screen2_content div[data-model="' + m + '"]:first').position().left + $('#screen2_content').scrollLeft();

        var duration = Math.abs(($('#screen2_content').scrollLeft() - scrollPos) / 5);

        $('#screen2_content').animate({scrollLeft: scrollPos}, duration);
    });

    // Set the current active screen in memory
    activeScreen = $('#linkidle').get(0);

    var containerBack = $('.container_footer_back');
    // To set scroll position back to correct width if user goes from screen 3 to 2
    $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {

        if (e.target.id === "linksearch") {
            filterSearch(false);
        }

        if (e.target.id === "linkscreen4") {

            if (!g_blnSiteplanInitialized) {
                initD3();
            } else {
                buildSessionOverlay();
            }

            var strActiveMenuItem = $('a.activeMenu').get(0).id;

            if ((strActiveMenuItem === "linkscreen1" || strActiveMenuItem === "linkcompare")
                    && g_intSelectedProductLine
                    && g_intSelectedModel
                    && g_intSelectedModelOptions) {

                buildHomesites();
                g_dataTableLots.draw();

                // Set colors
                $('#datatable_available_lots tr').css('background-color', PLCOLORS_INFO[g_intSelectedProductLine]);
                if (svg)
                    reset();
            }

            if (!$("#linkscreen4").hasClass('#activeMenu') && g_intSelectedLot) {
                $(g_colLotTable[g_intSelectedLot]).trigger('click');
            }
        }

        if (e.target.id === "linksearch") {
            $("#screensearch_content_top_row").empty();
            $("#screensearch_content_bottom_row").empty();
            buildScreenSearch();
        }

        if (e.target.id === "linkscreen2"
                || e.target.id === "linksearch") {
            $(".container_content").scrollLeft(scrollPosition);
            $("#screen2_content").scrollLeft(scrollPosition);
        } else {
            $(".container_content").scrollLeft(0);
        }
    });

    /* All tab functions */
    $('#linkscreen1').click(function (e) {
        
        buildProductLines();
        switchTab(this);
        containerBack.css('visibility', 'hidden');
        scrollPosition = 0;
    });

    $('#screenidle').on('click', 'img', function (e) {
        $('#linkscreen1').trigger('click');
    });

    $('#linkcompare').click(function (e) {
        switchTab(this);
        containerBack.css('visibility', 'hidden');

        if (compare1) {
            $('#compare1_backdrop').hide();
        }

        if (compare2) {
            $('#compare2_backdrop').hide();
        }
    });

    $('#linksearch').click(function (e) {
        switchTab(this);
        containerBack.css('visibility', 'hidden');
    });

    // Special case, it's faster to hide and show this screen instead of always rendering again everytime the user views the siteplan
    $('#linkscreen4').on('click', function (e, blnSitePlan = true) {

        // Clear pages for extra space, improves performance a lot
        $('#screen1_content').empty();
        $('#screen2_content_top_row').empty();
        $('#screen2_content_bottom_row').empty();
        $('#screen3_content').empty();
        $('#screensearch_content_top_row').empty();
        $('#screensearch_content_bottom_row').empty();

        $(this).tab('show');
        activeScreen = this;

        clearPulseSVG();


        if (blnSitePlan) {
            $('a.activeMenu').removeClass('activeMenu');
            $(this).addClass('activeMenu');
            removeSvgStyle();
            applySvgStyle($("#linkscreen4").get(0));
            $("#panel_homesites").hide();

            if (g_intSelectedLot) {

                $(".panel_siteplan_info").hide();
                $("#panel_siteplan").show();
            } else {
                $(".panel_siteplan_info").show();
                $("#panel_siteplan").hide();
                reset();
            }

            containerBack.css('visibility', 'hidden');
        } else {
            containerBack.css('visibility', 'visible');
    }
    });

    // Hidden tab links
    $('#linkscreen2').click(function (e) {
        buildAvailableModels();
        $(this).tab('show');
        activeScreen = this;
        containerBack.css('visibility', 'visible');
    });

    $('#linkscreen3').click(function (e) {
        buildModelDetails();
        $(this).tab('show');
        activeScreen = this;
        containerBack.css('visibility', 'visible');
    });

    $("#compare_container_clear").on('click', function () {
        compare1 = null;
        compare2 = null;
        $('#compare1_content').empty();
        $('#compare2_content').empty();
        $('#compare1_backdrop').show();
        $('#compare2_backdrop').show();
    });

    $(".compare_backdrop").on('click', function () {
        $('#linkscreen1').trigger('click');
    });
}

function initSearchPage() {
    // Default value
    pl = $('.search_group_pl .activeButton').attr('data-val');
    bedrooms = $('.search_group_bedrooms .activeButton').attr('data-val');
    bathrooms = $('.search_group_bathrooms .activeButton').attr('data-val');

    // when a button is selected, refresh the search results
    $(".div_filters_container").on('click', '.search_btn_option', function (e) {

        if ($(this).parent().hasClass('search_group_pl')) {
            pl = $(this).attr('data-val');
        }

        if ($(this).parent().hasClass('search_group_bedrooms')) {
            bedrooms = $(this).attr('data-val');
        }

        if ($(this).parent().hasClass('search_group_bathrooms')) {
            bathrooms = $(this).attr('data-val');
        }

        if ($(this).parent().hasClass('filters_clear_counter_container')) {

            if ($(this).is("#filters_btn_clear")) {
                bedrooms = 'all';
                bathrooms = 'all';
                pl = 'all';

                intSearchMaxArea = maxAvailableArea;
                intSearchMinArea = minAvailableArea;
                intSearchMaxPrice = maxAvailablePrice;
                intSearchMinPrice = minAvailablePrice;

                refreshSliders();
            }
        } else {
            $(this).siblings().removeClass('activeButton');
            $(this).addClass('activeButton');
        }

        filterSearch(false);
        $('#screensearch_content').scrollLeft(0);
    });
}

function initDataTables() {
    // the datatable for available lots
    g_dataTableLots = $('#datatable_available_lots').DataTable({
        data: [],
        paging: false,
        searching: false,
        info: false,
        "columns": [
            {"data": "name"},
            {"data": "price"},
            {"data": "grading"},
            {"data": "closingdate"}
        ],
        "order": [[1, "asc"]],
        'createdRow': function (row, data, dataIndex) {
            $(row).attr('id', data.number);
        },
        "scrollCollapse": true,
        "scrollY": "254px",
        select: 'single',
        responsive: false
    });

    // the datatable for available models
    g_dataTableModels = $('#datatable_available_models').DataTable({
        data: [],
        paging: false,
        searching: false,
        info: false,
        "columns": [
            {"data": "name"},
            {"data": "size"},
            {"data": "bathrooms"},
            {"data": "bedrooms"},
            {"data": "price"}
        ],
        "order": [[4, "asc"]],
        'createdRow': function (row, data, dataIndex) {
            $(row).attr('idPL', data.area);
            $(row).attr('idC', data.code);
            $(row).attr('idM', data.elevation);
        },
        "scrollCollapse": true,
        "scrollY": "522px",
        select: 'single',
        responsive: false
    });
}

function initSVGIcons() {
    // Ensure all the icons that will be used multiple times are loaded before creating the first page
    $.when(
            $.get(ICON_VIEW_URL),
            $.get(ICON_AREA_URL),
            $.get(ICON_COIN_URL),
            $.get(ICON_BATHUB_URL),
            $.get(ICON_BED_URL),
            $.get(ICON_DIAMOND_URL),
            $.get(ICON_HOMESITES_URL),
            $.get(ICON_SOLDHOUSE_URL))
            .done(function (
                    go
                    , area
                    , coin
                    , bathub
                    , bed
                    , diamond
                    , homesites
                    , soldhouse) {


                try {
                    g_IconView = go[0].querySelector('svg');
                    g_IconArea = area[0].querySelector('svg');
                    g_IconCoin = coin[0].querySelector('svg');
                    g_IconBath = bathub[0].querySelector('svg');
                    g_IconBed = bed[0].querySelector('svg');
                    g_IconDiamond = diamond[0].querySelector('svg');
                    g_IconHomesites = homesites[0].querySelector('svg');
                    g_IconSoldHouse = soldhouse[0].querySelector('svg');
                } catch (e) {
                    g_IconView = $.parseXML(go[0]).querySelector('svg');
                    g_IconArea = $.parseXML(area[0]).querySelector('svg');
                    g_IconCoin = $.parseXML(coin[0]).querySelector('svg');
                    g_IconBath = $.parseXML(bathub[0]).querySelector('svg');
                    g_IconBed = $.parseXML(bed[0]).querySelector('svg');
                    g_IconDiamond = $.parseXML(diamond[0]).querySelector('svg');
                    g_IconHomesites = $.parseXML(homesites[0]).querySelector('svg');
                    g_IconSoldHouse = $.parseXML(soldhouse[0]).querySelector('svg');
                }

                buildProductLines();

                var sessionList = g_fbdatabase.ref('lotsessions/');
                sessionList.on('value', function (snapshot) {
                    g_colLotSessions = snapshot.val();
                    changedValues();
                }, function (error) {
                    console.error(error);
                });

                var colConnections = g_fbdatabase.ref('lotconnections/');
                colConnections.on('value', function (snapshot) {
                    g_colAC = snapshot.val();
                    changedValues();
                }, function (error) {
                    console.error(error);
                });

                var colLots = g_fbdatabase.ref('lots/');
                colLots.on('value', function (snapshot) {
                    g_colLots = snapshot.val();
                    changedValues();
                }, function (error) {
                    console.error(error);
                });

                var colModels = g_fbdatabase.ref('models/');
                colModels.on('value', function (snapshot) {
                    g_colModels = snapshot.val();
                    changedValues();
                }, function (error) {
                    console.error(error);
                });


            }).fail(function () {
        console.log('Could not load svg icons');
    });
}

function changedValues() {
    initFilteredModels();
    switch (activeScreen.id) {
        case "linkscreen1":
            $('#linkscreen1').trigger("click");
            break;
        case "linksearch":
            $('#linksearch').trigger("click");
            break;
        case "linkscreen2":
            $('#linkscreen2').trigger("click");
            break;
        case "linkscreen3":
            $('#linkscreen3').trigger("click");
            break;
        case "linkscreen4":
            $('#linkscreen4').trigger("click");
            buildSessionOverlay();
            break;
        case "linkcompare":
            $('#linkcompare').trigger("click");
            break;
    }
}

$(document).ready(function () {
    $(document).idle({
        onIdle: function(){
            resetSitePlan();
        },
        idle: 1800000
      })
    $.when(fetchTablesOnce()).then(function (results) {
        g_colModels = results[0];
        g_colLots = results[1];
        g_colLotSessions = results[2];
        g_colAC = results[3];

        initFilteredModels();
        initMenuItems();
        initSliders();
        initSVGIcons();
        initDataTables();
        initSearchPage();
        
    });
});

function findAvailableLotsAC(strPL, strModelCode, strElevation) {
    // For performance, filter out the biggest and easiest to find elements first
    // Order of filtering: lot - Model - Elevation - AC rules
    // Step 1: Check available lots based on size
    // Step 2: check connected lots for nearby blocked models / elevations
    // Step 4: Push all the available lots to the array

    var availableLots = filterAvailableLots(strPL);
    var colBlockedLots = [];

    for (var i in availableLots) {
        for (var k in g_colLots[availableLots[i]].connections) {
            for (var l in g_colAC[k].rules) {

                var bm = [];
                var be = [];
                var bme = [];

                if (g_colAC[k].rules[l].blockedmodels) {
                    bm = g_colAC[k].rules[l].blockedmodels;
                }

                if (g_colAC[k].rules[l].blockedelevations) {
                    be = g_colAC[k].rules[l].blockedelevations;
                }

                if (g_colAC[k].rules[l].blockedmodelelevation) {
                    bme = g_colAC[k].rules[l].blockedmodelelevation;
                }

                // For each rule, add this combination or model or elevation to the block list
                // If rule 1 has a combination of given model and elevation, filter out by setting to 0
                if (matchingBlockedModel(bme, strModelCode + '_' + strElevation)) {
                    availableLots[i] = 0;
                }
            }
        }
    }

    // Find the difference betzeen the two, this is what will be available, ES6 function
    let difference = availableLots
            .filter(x => colBlockedLots.indexOf(x) == -1)
            .concat(colBlockedLots.filter(x => availableLots.indexOf(x) == -1));

    return difference;
}

function matchingBlockedModel(bme, strModelElevation) {

    for (var j in bme) {
        if (bme[j] == true && strModelElevation === j) {
            return true;
            break;
        }
    }

    return false;
}

// Filters out models that you can't buy anymore
function buildAvailableModels() {

    $("#screen2_content_top_row").empty();
    $("#screen2_content_bottom_row").empty();
    var objPL = g_colModels[g_intSelectedProductLine];

    // This variable will be toggled true / false so the content will alternate between top and bottom row
    var blnTopRow = true;
    var colOrdered = {};

    $('#screen2_menu').empty();
    // Order by sq ft
    for (var k in objPL) {

        colOrdered[k] = [];
        for (var l in objPL[k]) {
            colOrdered[k].push(objPL[k][l]);
        }

        /*
         colOrdered[k].sort(function (a, b) {
         return parseInt(a.size) - parseInt(b.size);
         });
         */


        var rangesqft = '';

        // console.log(k);
        // Crystal and Peridot exceptions to show range

        /**WOODHAVEN VERSION 1 CODE */
        // if (k === '36-1' || k === '36-3') {
        //     var maxSqFt = parseInt(colOrdered[k][0].size);

        //     for (var o in objPL[k]) {
        //         if (objPL[k][o].opt && parseInt(objPL[k][o].opt.size) > maxSqFt) {
        //             maxSqFt = objPL[k][o].opt.size;
        //         }
        //     }
        //     rangesqft = ' - ' + maxSqFt;
        // }

        $('#screen2_menu').append('<div data-model="' + k + '" class="screen2_menu_item" style="background-color:' + PLCOLORS[g_intSelectedProductLine] + '">'
                + objPL[k]['A'].model
                + '<br>'
                + colOrdered[k][0].size + rangesqft + ' SQ. FT.'
                + '</div>');
    }

    $('#screen2_menu div').hide();

    var availableModels = []
    var i=0;
    for (var k in colOrdered) {
        i++;
        availableModels[k] = []
        for (var l in colOrdered[k]) {
            
            var elev = colOrdered[k][l].elevation;
            var intLotsAvailable = g_colAvailable[k][elev + '_count'];
            var ModelDataEntry = {}
            if (intLotsAvailable > 0 && elev) {
            
                //MAP MODEL-CODE AS KEY VALUE CONTAINING ARRAY OF AVAIBLE MODELS
                ModelDataEntry = objPL[k][elev];
                availableModels[k].push(ModelDataEntry)
                
                $('#screen2_menu div[data-model="' + k + '"').show();

            }
        }
        if (blnTopRow) {
            var objTemplateTop = $('#screen2_template_models').clone(false).appendTo("#screen2_content_top_row");
            fillAvailableModelTemplate(objTemplateTop, g_intSelectedProductLine, k, availableModels[k]);
            objTemplateTop.addClass('col-xl-6');
            objTemplateTop.attr('data-model', k);
            blnTopRow = false;
        } else {
            var objTemplateBot = $('#screen2_template_models').clone(false).appendTo("#screen2_content_bottom_row");
            fillAvailableModelTemplate(objTemplateBot, g_intSelectedProductLine, k, availableModels[k]);
            objTemplateBot.addClass('col-xl-6');
            objTemplateTop.attr('data-model', k);
            blnTopRow = true;
        }
    }
    i<=4
        ?$('.screen2_arrow').hide()
        :$('.screen2_arrow').show()
    
    $(".screen3_wrapper_homesites").show();
}
function buildScreenSearch() {

    // This variable will be toggled true / false so the content will alternate between top and bottom row
    var blnTopRow = true;

    for (var k in g_colAvailable) {

        var objPL = g_colAvailable[k];
        for (var l in objPL) {

            var intLotsAvailable = g_colAvailable[k][l + '_count'];

            var pl = k.split('-')[0];

            if (intLotsAvailable > 0) {
                if (blnTopRow) {
                    var objTemplateTop = $('#screen2_template_models').clone(false).appendTo("#screensearch_content_top_row");
                    fillAvailableModelTemplate(objTemplateTop, pl, k, g_colModels[pl][k][l]);
                    objTemplateTop.attr("id", 'search_' + k + '-' + l);
                    objTemplateTop.addClass('col-xl-6');
                    blnTopRow = false;
                } else {
                    var objTemplateBot = $('#screen2_template_models').clone(false).appendTo("#screensearch_content_bottom_row");
                    fillAvailableModelTemplate(objTemplateBot, pl, k, g_colModels[pl][k][l]);
                    objTemplateBot.attr("id", 'search_' + k + '-' + l);
                    objTemplateBot.addClass('col-xl-6');
                    blnTopRow = true;
                }
            }
        }
    }

    filterSearch(false);
}
/**
 * 
 * @param {type} objTemplate
 * @param {type} pl is the pl number eg 30
 * @param {type} k is the index is the model list code eg 30-1
 * @param {type} objModel
 * @returns {undefined}
 */
function fillAvailableModelTemplate(objTemplate, pl, k, objModel) {
    if(!Array.isArray(objModel)){

        var strCode = objModel.code;
        var strModel = objModel.model;
        var strElevation = objModel.elevation;
        var strName = pl + '&rsquo;';
        var intPrice = objModel.price;
        var intArea = objModel.size;
        var intBedrooms = objModel.bedrooms;
        var strBathrooms = objModel.bathrooms;

        objTemplate.show();
        objTemplate.id = "";

        var objContainerName = objTemplate.find(".template_div_details_name_container");
        objContainerName.css('background-color', PLCOLORS[pl]);
        // Clone our current button and diamond in memory, manipulate and add to view
        var objButtonCircle = objTemplate.find(".screen2_container_icon_button");
        var svgButton = g_IconView.cloneNode(true);
        svgButton.getElementById(ICON_VIEW_CHANGE).style.fill = PLCOLORS[pl];
        objButtonCircle.html(svgButton);
        var objDiamond = objTemplate.find(".template_container_icon_diamond");
        var svgDiamond = g_IconDiamond.cloneNode(true);
        svgDiamond.getElementById(ICON_DIAMOND_CHANGE).style.fill = PLCOLORS[pl];
        objDiamond.html(svgDiamond);

        // Clone our svg and append to div, div dimensions changed the icon dimensions
        var objIconArea = objTemplate.find(".screen2_container_icon_area");
        objIconArea.html(g_IconArea.cloneNode(true));
        var objIconPrice = objTemplate.find(".screen2_container_icon_price");
        objIconPrice.html(g_IconCoin.cloneNode(true));
        var objIconBath = objTemplate.find(".screen2_container_icon_bathrooms");
        objIconBath.html(g_IconBath.cloneNode(true));
        var objIconBed = objTemplate.find(".screen2_container_icon_bedrooms");
        objIconBed.html(g_IconBed.cloneNode(true));

        // Background colors for info panel
        var objContainerName = objTemplate.find(".template_div_details_info_container");
        objContainerName.css('background-color', PLCOLORS_INFO[pl]);
    
        if(strName == '30&rsquo;'){
            strName ='30&rsquo; REAR LANE';
        }
        if(strName == '31&rsquo;'){
            strName ='30&rsquo; DETACHED';
        }
        objTemplate.find(".template_span_pl_name").html(strName + ' | ' + strModel+ ' '+strElevation);
        objTemplate.find(".template_div_details_housetype").html(strModel).css("color", PLCOLORS[pl]);
    
        objTemplate.find(".screen2_span_models_sizeMin").html(addCommas(intArea)+' <span>SQFT</span>');
        objTemplate.find(".screen2_span_models_priceMin").html(addCommas(intPrice));
    
        if (objModel.opt && !isNaN(parseFloat(objModel.opt.bathrooms))) {
            strBathrooms += ' - ' + objModel.opt.bathrooms;
        }
    
        if (objModel.opt && !isNaN(parseInt(objModel.opt.bedrooms))) {
            intBedrooms += ' - ' + objModel.opt.bedrooms;
        }
    
        objTemplate.find(".screen2_span_models_bathrooms").html(strBathrooms);
        objTemplate.find(".screen2_span_models_bedrooms").html(intBedrooms);
    
        var singleElev = '';
        if(strElevation.includes('Corner')){
            singleElev = strElevation.replace(' Corner', '');
        }else if(strElevation.includes('End')){
            singleElev = strElevation.replace(' End', '')
        }else{
            singleElev = strElevation
        }
        objTemplate.find(".screen2_img_model").attr("src", "img/models_small/"+strCode+"-"+singleElev+".jpg");
    
        objTemplate.on("click", {id: k, name: strName, elevation: strElevation, selectedpl: pl}, function (event) {
            g_intSelectedModel = event.data.id;
            g_intSelectedModelOptions = event.data.elevation;
            g_intSelectedProductLine = event.data.selectedpl;
    
            $('#linkscreen3').trigger("click");
        }), {passive: true};
    }else{
        if (objModel.length > 0) {
            var minSize = Math.min.apply(Math, objModel.map(function (o) {
                if (o.opt.size < o.size) {
                    return o.opt.size
                } else {
                    return o.size
                }
            }))
            var maxSize = Math.max.apply(Math, objModel.map(function (o) {
                if (o.opt.size > o.size) {
                    return o.opt.size
                } else {
                    return o.size
                }
            }))
            var minPrice = Math.min.apply(Math, objModel.map(function (o) {
                if (o.opt.price < o.price) {
                    return o.opt.price
                } else {
                    return o.price
                }
            }))
            var maxPrice = Math.max.apply(Math, objModel.map(function (o) {
                if (o.opt.price > o.price) {
                    return o.opt.price
                } else {
                    return o.price
                }
            }))
            var minBedrooms = Math.min.apply(Math, objModel.map(function (o) {
                if (o.opt.bedrooms < o.bedrooms) {
                    return o.opt.bedrooms
                } else {
                    return o.bedrooms
                }
            }))
            var maxBedrooms = Math.max.apply(Math, objModel.map(function (o) {
                if (o.opt.bedrooms > o.bedrooms) {
                    return o.opt.bedrooms
                } else {
                    return o.bedrooms
                }
            }))
            var minBathrooms = Math.min.apply(Math, objModel.map(function (o) {
                if (o.opt.bathrooms < o.bathrooms) {
                    return o.opt.bathrooms;
                } else {
                    return o.bathrooms;
                }
            }))
            var maxBathrooms = Math.max.apply(Math, objModel.map(function (o) {
                if (o.opt.bathrooms > o.bathrooms) {
                    return o.opt.bathrooms
                } else {
                    return o.bathrooms
                }

                // if(o.bathrooms > o.opt.bathrooms ){
                //     return o.bathrooms;
                // }else{
                //     return o.opt.bathrooms;
                // }            
            }))

            var strCode = objModel[0].code;
            var strModel = objModel[0].model;
            var strElevation = objModel[0].elevation;
            var strName = pl + '&rsquo;';
            var intMinArea = minSize;
            var intMaxArea = maxSize;
            var intMinPrice = minPrice;
            var intMaxPrice = maxPrice;

            var intMinBedrooms = minBedrooms;
            var intMaxBedrooms = maxBedrooms;

            var intMinBathrooms = minBathrooms;
            var intMaxBathrooms = maxBathrooms;

            objTemplate.show();
            objTemplate.id = "";

            var objContainerName = objTemplate.find(".template_div_details_name_container");
            objContainerName.css('background-color', PLCOLORS[pl]);

            // Clone our current button and diamond in memory, manipulate and add to view
            var objButtonCircle = objTemplate.find(".screen2_container_icon_button");
            var svgButton = g_IconView.cloneNode(true);
            svgButton.getElementById(ICON_VIEW_CHANGE).style.fill = PLCOLORS[pl];
            objButtonCircle.html(svgButton);

            var objDiamond = objTemplate.find(".template_container_icon_diamond");
            var svgDiamond = g_IconDiamond.cloneNode(true);
            svgDiamond.getElementById(ICON_DIAMOND_CHANGE).style.fill = PLCOLORS[pl];
            objDiamond.html(svgDiamond);

            // Clone our svg and append to div, div dimensions changed the icon dimensions
            var objIconArea = objTemplate.find(".screen2_container_icon_area");
            objIconArea.html(g_IconArea.cloneNode(true));
            var objIconPrice = objTemplate.find(".screen2_container_icon_price");
            objIconPrice.html(g_IconCoin.cloneNode(true));
            var objIconBath = objTemplate.find(".screen2_container_icon_bathrooms");
            objIconBath.html(g_IconBath.cloneNode(true));
            var objIconBed = objTemplate.find(".screen2_container_icon_bedrooms");
            objIconBed.html(g_IconBed.cloneNode(true));

            // Background colors for info panel
            var objContainerName = objTemplate.find(".template_div_details_info_container");
            objContainerName.css('background-color', PLCOLORS_INFO[pl]);
            
            if(strName == '30&rsquo;'){
                strName ='30&rsquo; RL';
            }
            if(strName == '31&rsquo;'){
                strName ='30&rsquo;';
            }
            objTemplate.find(".template_span_pl_name").html(strName + ' | ' + strModel);
            objTemplate.find(".template_div_details_housestyle").html(strElevation);
            objTemplate.find(".template_div_details_housetype").html(strModel).css("color", PLCOLORS[pl]);

            //console.log(MaxValuesPerModel)
            intMinArea == intMaxArea ?
                objTemplate.find(".screen2_span_models_sizeMin").html(addCommas(intMinArea) + " <span>SQ.FT.</span>") :
                objTemplate.find(".screen2_span_models_sizeMax").html(addCommas(intMinArea) + " - " + addCommas(intMaxArea) + " <span>SQ.FT.</span>");

            intMinPrice == intMaxPrice ?
                objTemplate.find(".screen2_span_models_priceMin").html(addCommas(intMinPrice)) :
                objTemplate.find(".screen2_span_models_priceMax").html(addCommas(intMinPrice) + ' - ' + addCommas(intMaxPrice));

            if (objModel.opt && !isNaN(parseFloat(objModel.opt.bathrooms))) {
                strBathrooms += ' - ' + objModel.opt.bathrooms;
            }

            if (objModel.opt && !isNaN(parseInt(objModel.opt.bedrooms))) {
                intBedrooms += ' - ' + objModel.opt.bedrooms;
            }

            /**IF BATHROOMS AND BEDROOMS HAVE SAME VALUE ONLY SHOW ONCE */
            intMinBathrooms == intMaxBathrooms ?
                objTemplate.find(".screen2_span_models_bathrooms").html(intMinBathrooms) :
                objTemplate.find(".screen2_span_models_bathrooms").html(intMinBathrooms + ' - ' + intMaxBathrooms);

            intMinBedrooms == intMaxBedrooms ?
                objTemplate.find(".screen2_span_models_bedrooms").html(intMinBedrooms) :
                objTemplate.find(".screen2_span_models_bedrooms").html(intMinBedrooms + ' - ' + intMaxBedrooms);

                var singleElev = '';
                if(strElevation.includes('Corner')){
                    singleElev = strElevation.replace('Corner', '');
                }else if(strElevation.includes('End')){
                    singleElev = strElevation.replace('End', '')
                }else{
                    singleElev = strElevation
                }
            objTemplate.find(".screen2_img_model").attr("src", "img/models_small/"+strCode+"-"+singleElev+".jpg");

            objTemplate.on("click", {
                id: k,
                name: strName,
                elevation: strElevation,
                selectedpl: pl
            }, function (event) {
                g_intSelectedModel = event.data.id;
                g_intSelectedModelOptions = event.data.elevation;
                g_intSelectedProductLine = event.data.selectedpl;
                $('#linkscreen3').trigger("click");
            }), {
                passive: true
            };

        }
    }
}
function buildModelDetails() {
    $("#screen3_content").empty();

    scrollPosition = $("#screen2_content").scrollLeft();

    var objM = g_colModels[g_intSelectedProductLine][g_intSelectedModel];
    var objMO = g_colModels[g_intSelectedProductLine][g_intSelectedModel][g_intSelectedModelOptions];
    var strCode = objMO.code;
    var strName = objMO.model;
    var strElevation = objMO.elevation;

    var objTemplate = $('#screen3_template_details').clone(false).appendTo("#screen3_content");
    objTemplate.id = "";

    // Add flexbox and column size to template dynamically;
    objTemplate.addClass('col-xl-4');
    var singleElev = '';
    if(strElevation.includes('Corner')){
        singleElev = strElevation.replace('Corner', '');
    }else if(strElevation.includes('End')){
        singleElev = strElevation.replace('End', '')
    }else{
        singleElev = strElevation
    }

    objTemplate.find(".screen3_img_model").css("background-image", "url(img/models_large/" + strCode + "-" + singleElev.replace(/\s/g, '') + ".jpg)");
    objTemplate.find(".screen3_image_url").attr("href", "img/models_large/" + strCode + "-" + singleElev.replace(/\s/g, '') + ".jpg");


    // set colors
    var objContainerPL = objTemplate.find(".screen3_pl_name_container");
    objContainerPL.css('background-color', PLCOLORS[g_intSelectedProductLine]);

    // set pl
    var strPL = g_intSelectedProductLine + "&rsquo;";
    if(strPL == '30&rsquo;'){
        strPL ='30&rsquo; REAR LANE';
    }
    if(strPL == '31&rsquo;'){
        strPL ='30&rsquo;';
    }
    objTemplate.find(".screen3_span_pl_name").html(strPL+ ' | '+strName);
    // objTemplate.find(".screen3_div_details_housetype").html();

    var objAccItem = objTemplate.find(".screen3_template_accitem");

    for (var l in g_colModels[g_intSelectedProductLine][g_intSelectedModel]) {
        var intLotsAvailable = 0;
        var availableLots = findAvailableLotsAC(g_intSelectedProductLine, g_intSelectedModel, l);

        // Initialize this once at startup, on session change then make sure this refreshes
        // TODO: improve performance by init colAvailable at startup instead of checking on every screen3
        for (var p in availableLots) {
            if (availableLots.hasOwnProperty(p) && parseInt(availableLots[p]) !== 0) {
                intLotsAvailable++;
                g_colAvailable[l] = availableLots;
            }
        }

        g_colAvailable[l + '_count'] = intLotsAvailable;
    }

    // calculate content container width
    var widthContainer = 615;
    var intElevsAvailable = 0;
    var widthButtons = 0;
    var widthContent = 0;
    for (var k in objM) {
        var strE = objM[k].elevation;

        if (g_colAvailable[strE + '_count'] > 0) {
            intElevsAvailable++;
        }
    }

    // 50 is size of the buttons
    widthButtons = intElevsAvailable * 50;
    widthContent = widthContainer /*- widthButtons*/;

    /**Hide townhome models with no rendering */

    // console.log(CountHideDetails);
    for (var k in objM) {
        // check if this has more than 1 available, then add ass viewable option
        if (g_colAvailable[objM[k].elevation + '_count'] > 0) {
            var objContainerItem = objTemplate.find(".screen3_elevs_content");
            var objItem = objAccItem.clone(false).appendTo(objContainerItem);
            var countHomesites;
            var strTitle = objM[k].elevation;

            var elevLetter = strTitle.split( ' ')[0];
            var elevOptional = strTitle.split( ' ')[1];
            elevOptional 
                ? objItem.find(".slideaccordion_title").html(elevLetter+'<small style="font-size: 60%"> '+elevOptional+'</small>')
                : objItem.find(".slideaccordion_title").html(elevLetter);
            
            strTitle.toLowerCase().endsWith('corner')
                ?objItem.find(".slideaccordion_title").addClass('screen3_corner')
                : ''

            objItem.css('display', 'inline-block');

            objItem.find('.screen3_template_accitem_top_container .slideaccordion_title').css('color', PLCOLORS[g_intSelectedProductLine]);

            objItem.on("click", {id: k, c: objContainerItem}, function (event) {
                $('.screen3_template_accitem').removeClass('active');
                $(this).addClass('active')

                if (g_intSelectedModelOptions !== event.data.id && !g_lockedAccordion) {

                    var objImage = objTemplate.find(".screen3_img_model");
                    var objImageUrl = objTemplate.find(".screen3_image_url");
                    var mainElevs = strCode + "-" + event.data.id.replace(/\s/g, '')

                    if((!mainElevs.includes('Corner') || !mainElevs.includes('End'))){
                        var cleanText = mainElevs
                        if(mainElevs.includes('Corner')){
                            cleanText = mainElevs.replace('Corner', '');
                        }else{
                            cleanText = mainElevs.replace('End', '');
                        }
                        objImage.css("background-image", "url(img/models_large/" + cleanText + ".jpg)");
                        objImageUrl.attr("href", "img/models_large/" + cleanText + ".jpg")
                    }else{
                        objImage.css("background-image", "url(img/models_large/" + mainElevs + ".jpg)");
                        objImageUrl.attr("href", "img/models_large/" + mainElevs + ".jpg")
                    }
                    
                    g_intSelectedModelOptions = event.data.id;
                    var objDataContainer = event.data.c.find('.screen3_template_accitem_content_container');

                    var objCopy = objDataContainer.clone(false);
                    // objCopy.css({'opacity': '0'});
                    $(this).after(objCopy);
                    objDataContainer.on("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function () {
                        $(this).remove();
                        g_lockedAccordion = false;
                        $('#screen3_content .screen3_template_div_floorplans').remove();


                        contentContainer.find(".screen3_wrapper_option");
                        AddFloorplans(false, $('#screen3_template_div_floorplans'), '.screen3_svg_floorplan', '#screen3_content', g_intSelectedModel);
                    });

                    g_lockedAccordion = true;
                    objDataContainer.css({'opacity': '0'});
                    countHomesites = g_colAvailable[event.data.id + '_count'];
                    fillScreen3Content(objCopy, widthContent, g_intSelectedProductLine, g_intSelectedModel, event.data.id, countHomesites, 'screen3');
                    // objCopy.css({'opacity': '1'});
                    objCopy.find('.screen3_wrapper_option').css('opacity', '1');
                }
            });

            if (objM[k].elevation === strElevation) {
                var contentContainer = objTemplate
                        .find('.screen3_template_accitem_content_container')
                        .clone(false)
                        .appendTo(objContainerItem);

                countHomesites = g_colAvailable[strElevation + '_count'];
                fillScreen3Content(contentContainer, widthContent, g_intSelectedProductLine, g_intSelectedModel, k, countHomesites, 'screen3');
                
            }
        }
        
    }

    AddFloorplans(false, $('#screen3_template_div_floorplans'), '.screen3_svg_floorplan', '#screen3_content', g_intSelectedModel);

    objTemplate.find(".comparesvg").off('click');
    objTemplate.find(".comparesvg").on('click', function () {
        addToCompare(objMO);
        $('#linkcompare').trigger('click');
    });

    objTemplate.show();
}


function addToCompare(objMO) {

    if (compare1 && compare2) {
        $('#compare1_replace').off('click');
        $('#compare2_replace').off('click');

        $('#compare1_replace').show();
        $('#compare2_replace').show();

        $('#compare1_replace').on('click', function () {
            $('#compare1_content').empty();
            buildCompareScreen(objMO, "#compare1_content");
            compare1 = objMO;
            $('#compare1_replace').hide();
            $('#compare2_replace').hide();
        });

        $('#compare2_replace').on('click', function () {
            $('#compare2_content').empty();
            buildCompareScreen(objMO, "#compare2_content");
            compare2 = objMO;
            $('#compare1_replace').hide();
            $('#compare2_replace').hide();
        });
    } else if (!compare1) {
        buildCompareScreen(objMO, "#compare1_content");
        $('#compare1_backdrop').hide();
        compare1 = objMO;
    } else if (!compare2) {
        buildCompareScreen(objMO, "#compare2_content");
        $('#compare1_backdrop').hide();
        compare2 = objMO;
    }
}

function buildCompareScreen(objMO, container) {
    $('#screencompare_floorplans').empty();
    AddFloorplans(false, $('#screencompare_template_div_floorplans'), '.screencompare_svg_floorplan', '#screencompare_floorplans', objMO.code);

    var objTemplate = $('.screencompare_template_container').eq(0).clone(false).appendTo(container);
    objTemplate.id = "";

    $(container).on('click', '#screencompare_floorplans', {id: objMO.code, elevation: objMO.elevation, selectedpl: objMO.area}, function (event) {

        g_intSelectedModel = event.data.id;
        g_intSelectedModelOptions = event.data.elevation;
        g_intSelectedProductLine = event.data.selectedpl;

        $('#linkscreen3').trigger("click");
    });

    // set colors
    var objContainerPL = objTemplate.find(".screencompare_pl_name_container");
    objContainerPL.css('background-color', PLCOLORS[objMO.area]);

    // set pl
    var strPL = objMO.area + "&rsquo;";
    if(strPL == '30&rsquo;'){
        strPL ='30&rsquo; REAR LANE';
    }
    if(strPL == '31&rsquo;'){
        strPL ='30&rsquo;';
    }
    objTemplate.find(".screencompare_span_pl_name").html(strPL+ ' | '+objMO.model);
    // objTemplate.find(".screen3_div_details_housetype").html();

    var countHomesites = g_colAvailable[objMO.elevation + '_count'];
    fillScreen3Content(objTemplate.find('.screencompare_details_container'), '100%', objMO.area, objMO.code, objMO.elevation, countHomesites, 'screencompare');

    var objContent = objTemplate.find(".screencompare_images_container");
    for (var k in g_colModels[objMO.area][objMO.code]) {

        
        var o = g_colModels[objMO.area][objMO.code][k];
        
        if (g_colAvailable[o.elevation + '_count'] > 0) {
            
            var imageContainer = objTemplate.find('.screencompare_images_container_template').eq(0).clone(false).appendTo(objContent);

            imageContainer.find(".compare_elevation_text").html(o.model +' '+ o.elevation);
            var img = imageContainer.find('.compare_model_image');

            var singleElev = '';
            if(o.elevation.includes('Corner')){
                singleElev = o.elevation.replace(' Corner', '');
            }else if(o.elevation.includes('End')){
                singleElev = o.elevation.replace(' End', '')
            }else{
                singleElev = o.elevation
            }

            img.attr('src', 'img/models_small/' + objMO.code + '-' + singleElev + '.jpg');

            if (o.elevation === objMO.elevation) {
                img.css('opacity', '1');
            }

            countHomesites = g_colAvailable[o.elevation + '_count'];

            img.on('click', {id: k, o: o, countH: countHomesites}, function (e) {
                console.log(e.data)
                fillScreen3Content(objTemplate.find('.screencompare_details_container'), '100%', e.data.o.area, e.data.o.code, e.data.id, e.data.countH, 'screencompare');

                var opt = objTemplate.find('.screen3_wrapper_option');
                objTemplate.find('img.compare_model_image').css('opacity', '0.5');

                var c = objTemplate.find('.screencompare_details_container');
                c.siblings().find('.screencompare_template_div_floorplans').remove();
                var container = c.siblings().eq(1);
                AddFloorplans(false, $('#screencompare_template_div_floorplans'), '.screencompare_svg_floorplan', container, e.data.o.code);

                opt.css('opacity', '1.0');
                $(this).css('opacity', '1');
            });

            imageContainer.show();

        }
    }

    objTemplate.show();
}

function AddFloorplans(blnOpt, containerTemp, containerChild, container, m) {
    // Main floor
    var objFPMain = containerTemp.clone(false).appendTo(container);
    objFPMain.addClass('container-fluid');

    if (blnOpt) {
        OptFPExists(m, objFPMain, 'ground');
    } else {
        
        objFPMain.find($(containerChild).parent()).attr("href", "img/models_floorplans/" + m + "_ground.svg");
        objFPMain.find(containerChild).attr("src", "img/models_floorplans/" + m + "_ground.svg");
    }

    // Second floor    
    var objFPSecondFloor = containerTemp.clone(false).appendTo(container);
    objFPSecondFloor.addClass('container-fluid');

    if (blnOpt) {
        OptFPExists(m, objFPSecondFloor, 'second');

        var objFPExtra = containerTemp.clone(false).appendTo(container);
        OptFPExists(m, objFPExtra, 'extra');
        objFPExtra.show();
    } else {
        objFPSecondFloor.find($(containerChild).parent()).attr("href", "img/models_floorplans/" + m + "_second.svg");
        objFPSecondFloor.find(containerChild).attr("src", "img/models_floorplans/" + m + "_second.svg");
    }

    // Basement
    var objFPBasement = containerTemp.clone(false).appendTo(container);
    objFPBasement.addClass('container-fluid');

    if (blnOpt) {
        OptFPExists(m, objFPBasement, 'basement');
    } else {
        objFPBasement.find($(containerChild).parent()).attr("href", "img/models_floorplans/" + m + "_basement.svg");
        objFPBasement.find(containerChild).attr("src", "img/models_floorplans/" + m + "_basement.svg");
    }

    objFPMain.show();
    objFPSecondFloor.show();
    objFPBasement.show();

    // End gutter for flex row
    $(".gutterright").remove();
    $("#screen3_content").append('<div class="gutterright container-fluid" style="height: 886px; width: 30px;"></div>');
}

function OptFPExists(model, objC, strFloor) {
    // console.log(model, objC, strFloor);
    $.get("img/models_floorplans/" + model + "_opt_" + strFloor + ".svg")
        .done(function () {
            objC.find("a").attr("href", "img/models_floorplans/" + model + "_opt_" + strFloor + ".svg");
            objC.find(".screen3_svg_floorplan").attr("src", "img/models_floorplans/" + model + "_opt_" + strFloor + ".svg");
            objC.find(".screencompare_svg_floorplan").attr("src", "img/models_floorplans/" + model + "_opt_" + strFloor + ".svg");
        }).fail(function () {

        objC.find(".screen3_svg_floorplan").attr("src", "img/models_floorplans/" + model + "_" + strFloor + ".svg");
        objC.find(".screencompare_svg_floorplan").attr("src", "img/models_floorplans/" + model + "_" + strFloor + ".svg");

        if (strFloor === 'extra') {
            objC.find(".screen3_svg_floorplan").parent().hide();
            objC.find(".screencompare_svg_floorplan").parent().hide();
        }
    }).promise();
}

function validateOptField(optField) {
    if (optField
            && optField.toString().toLowerCase() !== 'n/a'
            && optField.toString().toLowerCase() !== 'none') {
        return true;
    } else {
        return false;
    }
}

//Target defines if to add floorplans to screen3 or 
function fillScreen3Content(contentContainer, widthContent, pl, m, modelOptions, countHomesites, strTarget) {
    var objMO = g_colModels[pl][m][modelOptions];
    var intPrice = objMO.price;
    var intArea = objMO.size;
    var intBedrooms = objMO.bedrooms;
    var intBathrooms = objMO.bathrooms;

    var blnOpt = (objMO.opt && objMO.opt.name && objMO.opt.name !== '' && objMO.opt.name !== 'None' && objMO.opt.name !== 'N/A') ? true : false;

    addDiamond(contentContainer, '.screen3_container_icon_diamond', PLCOLORS[pl]);
    addArea(contentContainer, '.screen3_container_icon_area', PLCOLORS[pl]);
    addBath(contentContainer, '.screen3_container_icon_bathrooms', PLCOLORS[pl]);
    addPrice(contentContainer, '.screen3_container_icon_price', PLCOLORS[pl]);
    addBed(contentContainer, '.screen3_container_icon_bedrooms', PLCOLORS[pl]);

    // Add homesites
    // contentContainer.find(".screen3_wrapper_homesites").css('background-color', PLCOLORS[pl]);
    contentContainer.find(".screen3_wrapper_homesites").css('background-color', '#58595b');
    $('.screen3_elevs_content').find('.screen3_template_accitem').css({'background-color': ''})
    $('.screen3_elevs_content').find('.screen3_template_accitem .slideaccordion_title').css('color', PLCOLORS[pl])
    var prev = contentContainer[0].previousSibling;
    $(prev).css({'background-color':PLCOLORS[pl]})
    $(prev).find('.slideaccordion_title').css('color', '#fff')

    contentContainer.find(".screen3_container_homesites").html("VIEW<br><span>" + countHomesites + "</span> HOMESITES <i class='fa fa-chevron-right'></i>");
    contentContainer.find(".screen3_wrapper_homesites").on('click', {id: modelOptions, m: m, pl: pl}, function (event) {


        clearPulseSVG();
        g_intSelectedProductLine = event.data.pl;
        g_intSelectedModel = event.data.m;
        g_intSelectedModelOptions = event.data.id;
        $('#linkscreen4').trigger("click", false);
    });

    // Fill text
    contentContainer.find(".screen3_container_area").html(addCommas(intArea));
    contentContainer.find(".screen3_container_price").html(addCommas(intPrice));
    contentContainer.find(".screen3_container_bathrooms").html(intBathrooms);
    contentContainer.find(".screen3_container_bedrooms").html(intBedrooms);
    contentContainer.css('background-color', PLCOLORS_INFO[pl]);

    // Add option if option is available
    if (blnOpt) {
        var objOpt = objMO.opt;

        blnOptToggle = false;
        var sqftO = validateOptField(objOpt.size) ? objOpt.size : objMO.size;
        var priceO = validateOptField(objOpt.price) ? objOpt.price : objMO.price;
        var bathsO = validateOptField(objOpt.bathrooms) ? objOpt.bathrooms : objMO.bathrooms;
        var bedsO = validateOptField(objOpt.bedrooms) ? objOpt.bedrooms : objMO.bedrooms;

        var objOptContainer = contentContainer.find(".screen3_wrapper_option");
        objOptContainer.css('background-color', PLCOLORS[pl]);
        objOptContainer.find(".screen3_container_option").html(objOpt.name);

        var objButtonCircle = objOptContainer.find(".screen3_container_option_icon");
        var svgButton = g_IconView.cloneNode(true);
        svgButton.getElementById(ICON_VIEW_CHANGE).style.fill = PLCOLORS[pl];
        objButtonCircle.html(svgButton);

        objOptContainer.show();
        objOptContainer.attr('opt', 'false');

        objOptContainer.off('click');
        objOptContainer.on('click', function () {

            var scrollPos = $(".container_content").scrollLeft();

            if (!blnOptToggle) {
                blnOptToggle = true;
                if (objOpt.size)
                    contentContainer.find(".screen3_container_area").html(addCommas(sqftO));
                if (objOpt.price)
                    contentContainer.find(".screen3_container_price").html(addCommas(priceO));
                if (objOpt.bathrooms)
                    contentContainer.find(".screen3_container_bathrooms").html(bathsO);
                if (objOpt.bedrooms)
                    contentContainer.find(".screen3_container_bedrooms").html(bedsO);

                objOptContainer.find(".screen3_container_option").html('Remove option');
                objButtonCircle.find('svg').attr('transform', 'scale(-1 1) translate(0 0)');
                objOptContainer.css('opacity', 0.8);

                if (strTarget === 'screencompare') {
                    contentContainer.siblings().find('.screencompare_template_div_floorplans').remove();
                    var container = contentContainer.siblings().eq(1);
                    AddFloorplans(true, $('#screencompare_template_div_floorplans'), '.screencompare_svg_floorplan', container, m);
                }

                if (strTarget === 'screen3') {
                    $('#screen3_content .screen3_template_div_floorplans').remove();
                    AddFloorplans(true, $('#screen3_template_div_floorplans'), '.screen3_svg_floorplan', '#screen3_content', m);
                }
            } else {
                blnOptToggle = false;
                contentContainer.find(".screen3_container_area").html(addCommas(intArea));
                contentContainer.find(".screen3_container_price").html(addCommas(intPrice));
                contentContainer.find(".screen3_container_bathrooms").html(intBathrooms);
                contentContainer.find(".screen3_container_bedrooms").html(intBedrooms);
                objButtonCircle.find('svg').attr('transform', 'scale(1 1) translate(0 0)');
                objOptContainer.find(".screen3_container_option").html(objOpt.name);
                objOptContainer.css('opacity', 1);

                if (strTarget === 'screencompare') {
                    contentContainer.siblings().find('.screencompare_template_div_floorplans').remove();
                    var container = contentContainer.siblings().eq(1);
                    AddFloorplans(false, $('#screencompare_template_div_floorplans'), '.screencompare_svg_floorplan', container, m);
                }
                if (strTarget === 'screen3') {
                    $('#screen3_content .screen3_template_div_floorplans').remove();
                    AddFloorplans(false, $('#screen3_template_div_floorplans'), '.screen3_svg_floorplan', '#screen3_content', m);
                }
            }

            $(".container_content").scrollLeft(scrollPos);
        });
    } else {
        contentContainer.find(".screen3_wrapper_option").hide();
    }

    contentContainer.css({'width': widthContent + 'px', 'z-index': '9999'});
}

// returns an array of all the available lots given a product line number
function filterAvailableLots(intProductLine) {
    var availableLots = [];
    for (var k in g_colLots) {
        if (g_colLots.hasOwnProperty(k)) {

            if (parseInt(g_colLots[k].size) === parseInt(intProductLine)
                    && parseInt(g_colLotSessions[k].id_status) === STATUS.ID_AVAIL) {

                availableLots.push(k);
            }
        }
    }

    return availableLots;
}

// This function shows the selected elevation on the siteplan
function buildHomesites() {

    buildSessionOverlay();

    g_intSelectedLot = null;
    $("#panel_homesites").show();
    $("#panel_siteplan").hide();
    $(".panel_siteplan_info").hide();

    g_dataTableLots.clear();

    var objMO = g_colModels[g_intSelectedProductLine][g_intSelectedModel][g_intSelectedModelOptions];

    var colOptions = g_colAvailable[g_intSelectedModelOptions];
    for (var k in colOptions) {
        if (colOptions.hasOwnProperty(k) && parseInt(colOptions[k]) !== 0) {

            var rowL = g_colLots[colOptions[k]];
            var row = {};

            if (!rowL.premiums || isNaN(rowL.premiums)) {
                rowL.premiums = 0;
            }
            row.price = addCommas(parseInt(rowL.premiums) + parseInt(objMO.price));
            row.name = rowL.number.replace(/-Phase/g, ' PH');

            // Todo, add in database
            row.grading = rowL.grading || 'None';
            row.closingdate = rowL.closingdate || 'None';
            row.number = rowL.number;

            g_dataTableLots.row.add(row);

            if (g_blnSiteplanInitialized) {
                pulseSVG(g_colLotTable[rowL.number]);
            }
        }
    }

    var strName = objMO.model;
    var intPrice = objMO.price;
    var intArea = objMO.size;
    var intBedrooms = objMO.bedrooms;
    var intBathrooms = objMO.bathrooms;

    var singleElev = '';
    if(g_intSelectedModelOptions.includes('Corner')){
        singleElev = g_intSelectedModelOptions.replace(' Corner', '');
    }else if(g_intSelectedModelOptions.includes('End')){
        singleElev = g_intSelectedModelOptions.replace(' End', '')
    }else{
        singleElev = g_intSelectedModelOptions
    }
    
    $('#homesites_image').attr("src", "img/models_small/" + g_intSelectedModel + "-" + singleElev + ".jpg");

    $('#datatable_available_lots tbody').on('click', 'tr', function () {
        clicked(g_colLotTable[this.id], this.id);
        $(this).siblings().css('background-color', PLCOLORS_INFO[g_intSelectedProductLine]);
        $(this).css('background-color', '#58595b');
    });

    $(".screen4_details").css('background-color', PLCOLORS_INFO[g_intSelectedProductLine]);

    var contentContainer = $('#homesites_model_container');
    contentContainer.find('#homesites_name_container').css('background-color', PLCOLORS[g_intSelectedProductLine]);
    $('#homesites_name_container').css('background-color', PLCOLORS[g_intSelectedProductLine]);
    if(g_intSelectedProductLine == 31){
        g_intSelectedProductLine == 30
    }
    $('#homesites_name_container').find('#homesites_pl_name').html(g_intSelectedProductLine + "&rsquo; | " +strName + ' '+objMO.elevation);

    // Add svgs
    addDiamond(contentContainer, '.homesites_container_icon_diamond', PLCOLORS[g_intSelectedProductLine]);
    addArea(contentContainer, '.homesites_container_icon_area', PLCOLORS[g_intSelectedProductLine]);
    addBath(contentContainer, '.homesites_container_icon_bathrooms', PLCOLORS[g_intSelectedProductLine]);
    addPrice(contentContainer, '.homesites_container_icon_price', PLCOLORS[g_intSelectedProductLine]);
    addBed(contentContainer, '.homesites_container_icon_bedrooms', PLCOLORS[g_intSelectedProductLine]);

    // Fill text
    // contentContainer.find(".homesites_container_name").html(strName + ' STYLE ' + objMO.elevation);
    contentContainer.find(".homesites_container_area").html(addCommas(intArea));
    contentContainer.find(".homesites_container_price").html(addCommas(intPrice));
    contentContainer.find(".homesites_container_bathrooms").html(intBathrooms);
    contentContainer.find(".homesites_container_bedrooms").html(intBedrooms);
    contentContainer.css('background-color', PLCOLORS_INFO[g_intSelectedProductLine]);

    var objButtonFP = contentContainer.find('#homesites_floorplans');
    objButtonFP.css('background-color', PLCOLORS[g_intSelectedProductLine]);
    objButtonFP.off('click');
    objButtonFP.on('click', function () {
        backFunction();
    });
}

function addDiamond(Container, Child, Color) {
    var objDiamond = Container.find(Child);
    var svgDiamond = g_IconDiamond.cloneNode(true);
    svgDiamond.getElementById(ICON_DIAMOND_CHANGE).style.fill = Color;
    objDiamond.find('svg').remove();
    objDiamond.prepend(svgDiamond);
}

function addArea(Container, Child, Color) {
    var objArea = Container.find(Child);
    var svgArea = g_IconArea.cloneNode(true);
    svgArea.getElementById(ICON_AREA_CHANGE).style.fill = Color;
    objArea.find('svg').remove();
    objArea.prepend(svgArea);
}

function addPrice(Container, Child, Color) {
    var objPrice = Container.find(Child);
    var svgPrice = g_IconCoin.cloneNode(true);
    svgPrice.getElementById(ICON_PRICE_CHANGE).style.fill = Color;
    objPrice.find('svg').remove();
    objPrice.prepend(svgPrice);
}

function addBath(Container, Child, Color) {
    var objBaths = Container.find(Child);
    var svgBath = g_IconBath.cloneNode(true);
    svgBath.getElementById(ICON_BATH_CHANGE).style.fill = Color;
    objBaths.find('svg').remove();
    objBaths.prepend(svgBath);
}

function addBed(Container, Child, Color) {
    var objBeds = Container.find(Child);
    var svgBed = g_IconBed.cloneNode(true);
    svgBed.getElementById(ICON_BED_CHANGE).style.fill = Color;
    objBeds.find('svg').remove();
    objBeds.prepend(svgBed);
}

// This function maps all the models into a product line
// All product lines in the database are hash mapped by their area size
// All models lines in the database are hash mapped by code
// All lots are hashed by their lot id

function buildProductLines() {
    $("#screen1_content").empty();
    //$('#disclaimer_available_models').empty();
    scrollPosition = 0;
    for (var k in g_colModels) {
        if (g_colModels.hasOwnProperty(k)) {

            var intProductLineSize = parseInt(k);
            var strName = k + "&rsquo;";
            var intPriceMax = 0;
            var intPriceMin = 9999999;
            var intAreaMax = 0;
            var intAreaMin = 9999999;

            var intBedroomMax = 0;
            var intBedroomMin = 9999999;

            var intBathroomMax = 0;
            var intBathroomMin = 9999999;

            var intLotsAvailable = 0;
            var intLotsUnavailable = 0;
            var intLotsPending = 0;
            var intLotsSold = 0;

            var modelsCount = 0;

            // Step 1: Go through each lot to check if is the same size
            // Step 2: Go through lotsessions to check the lot status
            for (var l in g_colLots) {
                if (g_colLots.hasOwnProperty(l)) {
                    if (parseInt(g_colLots[l].size) === intProductLineSize && g_colLotSessions[l]) {
                        switch (parseInt(g_colLotSessions[l].id_status)) {
                            case STATUS.ID_AVAIL:
                                intLotsAvailable++;
                                break;
                            case STATUS.ID_UNAVAILABLE:
                                intLotsUnavailable++;
                                break;
                            case STATUS.ID_PENDING:
                                intLotsPending++;
                                break;
                            case STATUS.ID_SOLD:
                                intLotsSold++;
                                break;
                        }
                    }
                }
            }


            for (var l in g_colModels[k]) {
                for (var i in g_colModels[k][l]) {
                    if (g_colAvailable[l][i + "_count"] > 0) {

                        modelsCount++;
                        var intSize = parseInt(g_colModels[k][l][i].size);
                        var intPrice = parseInt(g_colModels[k][l][i].price);
                        var intBedroom = parseFloat(g_colModels[k][l][i].bedrooms);
                        var intBathroom = parseFloat(g_colModels[k][l][i].bathrooms);

                        if (intSize > intAreaMax) {
                            intAreaMax = intSize;
                        }

                        if (intSize < intAreaMin) {
                            intAreaMin = intSize;
                        }

                        if (intPrice > intPriceMax) {
                            intPriceMax = intPrice;
                        }

                        if (intPrice < intPriceMin) {
                            intPriceMin = intPrice;
                        }

                        if (intBedroom < intBedroomMin) {
                            intBedroomMin = intBedroom;
                        }

                        if (intBedroom > intBedroomMax) {
                            intBedroomMax = intBedroom;
                        }

                        if (intBathroom < intBathroomMin) {
                            intBathroomMin = intBathroom;
                        }

                        if (intBathroom > intBathroomMax) {
                            intBathroomMax = intBathroom;
                        }

                        if (g_colModels[k][l][i].opt) {
                            // If this model has options or alternatives and they have overwriting values
                            // Include the overwriting values of these into the ranges
                            if (g_colModels[k][l][i].opt.size) {
                                var intOptSize = parseInt(g_colModels[k][l][i].opt.size);

                                if (intOptSize > intAreaMax) {
                                    intAreaMax = intOptSize;
                                }

                                if (intOptSize < intAreaMin) {
                                    intAreaMin = intOptSize;
                                }
                            }

                            if (g_colModels[k][l][i].opt.price) {
                                var intOptPrice = parseInt(g_colModels[k][l][i].opt.price);
                                if (intOptPrice > intPriceMax) {
                                    intPriceMax = intOptPrice;
                                }

                                if (intOptPrice < intPriceMin) {
                                    intPriceMin = intOptPrice;
                                }
                            }

                            if (g_colModels[k][l][i].opt.bedrooms) {
                                var intOptBedroom = parseFloat(g_colModels[k][l][i].opt.bedrooms);
                                if (intOptBedroom > intBedroomMax) {
                                    intBedroomMax = intOptBedroom;
                                }

                                if (intOptBedroom < intBedroomMin) {
                                    intBedroomMin = intOptBedroom;
                                }
                            }

                            if (g_colModels[k][l][i].opt.bathrooms) {
                                var intOptBathroom = parseFloat(g_colModels[k][l][i].opt.bathrooms);

                                if (intOptBathroom > intBathroomMax) {
                                    intBathroomMax = intOptBathroom;
                                }

                                if (intOptBathroom < intBathroomMin) {
                                    intBathroomMin = intOptBathroom;
                                }
                            }
                        }
                    }
                }
            }

            if (modelsCount > 0) {

                var objTemplate = $('#screen1_template_productline').clone(false).appendTo("#screen1_content");
                objTemplate.show();
                objTemplate.id = "";

                /*
                 var strModelDisclaimer = '&nbsp;&amp;&nbsp;' + intProductLineSize + '&apos;';
                 
                 if($('#disclaimer_available_models').text() === ''){
                 strModelDisclaimer = intProductLineSize + '&apos;';
                 }                
                 
                 $('#disclaimer_available_models').append(strModelDisclaimer);
                 */

                // Set the template parts to their corresponding colors
                var objContainerName = objTemplate.find(".template_div_details_name_container");
                objContainerName.css('background-color', PLCOLORS[intProductLineSize]);

                // Clone our current button and diamond in memory, manipulate and add to view
                var objButtonCircle = objTemplate.find(".screen1_container_icon_button");
                var svgButton = g_IconView.cloneNode(true);
                svgButton.getElementById(ICON_VIEW_CHANGE).style.fill = PLCOLORS[intProductLineSize];
                objButtonCircle.html(svgButton);

                addDiamond(objTemplate, ".template_container_icon_diamond", PLCOLORS[intProductLineSize]);

                // Clone our svg and append to div, div dimensions changed the icon dimensions
                var objIconArea = objTemplate.find(".screen1_container_icon_area");
                objIconArea.html(g_IconArea.cloneNode(true));
                var objIconPrice = objTemplate.find(".screen1_container_icon_price");
                objIconPrice.html(g_IconCoin.cloneNode(true));
                var objIconBed = objTemplate.find(".screen1_container_icon_bed");
                objIconBed.html(g_IconCoin.cloneNode(true));
                var objIconBath = objTemplate.find(".screen1_container_icon_bath");
                objIconBath.html(g_IconCoin.cloneNode(true));
                // Background colors for info panel
                var objContainerName = objTemplate.find(".template_div_details_info_container");
                objContainerName.css('background-color', PLCOLORS_INFO[intProductLineSize]);

                var objImage = objTemplate.find(".screen1_img_pl_image");
                var objDetailsContainer = objTemplate.find(".screen1_template_div_details");

                // Cases for different product line designs
                switch (intProductLineSize) {
                    case 20:
                        objTemplate.addClass('col-xl-3');
                        objDetailsContainer.css("width", "157px");
                        objImage.attr("src", "img/pl_images/20.jpg");
                        break;

                    case 30:
                        objTemplate.addClass('col-xl-3');
                        objDetailsContainer.css("width", "157px");
                        objImage.attr("src", "img/pl_images/29.jpg");
                        break;
                    case 31:
                        objTemplate.addClass('col-xl-3');
                        objDetailsContainer.css("width", "157px");
                        objImage.attr("src", "img/pl_images/30.jpg");
                        break;
                    case 40:
                        objTemplate.addClass('col-xl-3');
                        objDetailsContainer.css("width", "157px");
                        objImage.attr("src", "img/pl_images/40.jpg");
                        break;
                    case 45:
                        objTemplate.addClass('col-xl-3');
                        objDetailsContainer.css("width", "157px");
                        objImage.attr("src", "img/pl_images/45.jpg");
                        break;
                }

                if (intPriceMin === 9999999) {
                    intPriceMin = 0;
                }
                if (intAreaMin === 9999999) {
                    intAreaMin = 0;
                }

                if (intPriceMin < minAvailablePrice) {
                    minAvailablePrice = intPriceMin;
                }
                if (intPriceMax > maxAvailablePrice) {
                    maxAvailablePrice = intPriceMax;
                }
                if (intAreaMin < minAvailableArea) {
                    minAvailableArea = intAreaMin;
                }
                if (intAreaMax > maxAvailableArea) {
                    maxAvailableArea = intAreaMax;
                }
                if (intBedroomMin < minAvailableBedroom) {
                    minAvailableBedroom = intBedroomMin;
                }
                if (intBedroomMax > maxAvailableBedroom) {
                    maxAvailableBedroom = intBedroomMax;
                }
                if (intBathroomMin < minAvailableBathroom) {
                    minAvailableBathroom = intBathroomMin;
                }
                if (intBathroomMax > maxAvailableBathroom) {
                    maxAvailableBathroom = intBathroomMax;
                }

                /*OPTIONAL ROOMS*/
                if (intOptBathroom > maxAvailableBathroom) {
                    maxAvailableBathroom = intBathroomMax;
                }

                if (minAvailableBathroom < intBathroomMin) {
                    intBathroomMin = intBathroomMin;
                }

                if (intOptBedroom > maxAvailableBedroom) {
                    maxAvailableBedroom = intBedroomMax;
                }

                if (intOptBedroom < minAvailableBedroom) {
                    minAvailableBedroom = intBedroomMin;
                }

                if(strName == "30&rsquo;"){
                    objTemplate.find(".template_span_pl_name").html("30'");
                    objTemplate.find(".header_title.header_text").html("REARLANE HOMES");
                }
                else if(strName == "31&rsquo;"){
                    objTemplate.find(".template_span_pl_name").html("30'");
                    objTemplate.find(".header_title.header_text").html("DETACHED HOMES");
                }
                else{
                    objTemplate.find(".template_span_pl_name").html(strName)
                }
                    

                if(intProductLineSize == 20){
                    objTemplate.find('.header_title.header_text').prepend('TOWN');
                }
                intAreaMin == intAreaMax?
                    objTemplate.find(". screen1_span_pl_sizeMin").html(addCommas(intAreaMin))
                :objTemplate.find(".screen1_span_pl_sizeMax").html(addCommas(intAreaMin)+' - ' +addCommas(intAreaMax));
                
                intPriceMin == intPriceMax ?
                    objTemplate.find(".screen1_span_pl_priceMin").html(addCommas(intPriceMin))
                :objTemplate.find(".screen1_span_pl_priceMax").html(addCommas(intPriceMin)+ ' - '+addCommas(intPriceMax));
                
                intBedroomMin == intBedroomMax ?
                    objTemplate.find(".screen1_span_pl_bedroomMax").html(addCommas(intBedroomMin))
                :objTemplate.find(".screen1_span_pl_bedroomMin").html(addCommas(intBedroomMin)+' - '+addCommas(intBedroomMax));

                intBathroomMin == intBathroomMax ?
                    objTemplate.find(".screen1_span_pl_bathroomMin").html(addCommas(intBathroomMin))
                :objTemplate.find(".screen1_span_pl_bathroomMax").html(addCommas(intBathroomMin)+' - '+addCommas(intBathroomMax))

                objTemplate.on("click", {id: k, name: strName}, function (event) {
                    
                    g_intSelectedProductLine = event.data.id;
                    $('#siteplan_select').html(event.data.name);
                    $('#linkscreen2').trigger("click");
                    $('.screen3_wrapper_homesites').show();
                });

                $('.search_group_pl div[data-val="' + intProductLineSize + '"]').show();
            } else {
                $('.search_group_pl div[data-val="' + intProductLineSize + '"]').hide();

                // Special case, if 60 is hidden change layout
                if (intProductLineSize == 60) {
                    $.each($('#screen1_content .col-xl-4'), function () {
                        $(this).removeClass('col-xl-4').addClass('col-xl-6');
                        $(this).find('.screen1_template_div_details').css("width", "200px");

                        var img = $(this).find('img');
                        img.attr("src", img.attr("src").split('.')[0] + '_large.jpg');
                    });
                }

            }
        }
    }

    intSearchMinArea = minAvailableArea;
    intSearchMaxArea = maxAvailableArea;
    intSearchMinPrice = minAvailablePrice;
    intSearchMaxPrice = maxAvailablePrice;

    $('#slider_area').data('slider-min', minAvailableArea);
    $('#slider_area').data('slider-max', maxAvailableArea);
    $('#slider_area').slider({
        min: minAvailableArea,
        max: maxAvailableArea,
        value: [minAvailableArea, maxAvailableArea]
    });

    $('#slider_price').data('slider-min', minAvailablePrice);
    $('#slider_price').data('slider-max', maxAvailablePrice);
    $('#slider_price').slider({
        min: minAvailablePrice,
        max: maxAvailablePrice,
        value: [minAvailablePrice, maxAvailablePrice]
    });

    refreshSliders();
}

// Acc commas to seperate thousands in numbers, for front end
function addCommas(intNumber) {
    return intNumber.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function zoomManually(value) {
    var val = parseFloat(value);
    if (val !== scale) {
        scale = val;
        svg.call(zoom.scaleTo, scale);
    }
}

function initializeSitePlan() {

    g_svg_siteplan.on('click', 'path, polygon', function () {

        if ($(this)[0].id) {

            var lotId = $(this)[0].id.split('_')[1];

            if (g_colLotSessions[lotId].id_status == STATUS.ID_AVAIL) {
                if ($('a.activeMenu').get(0).id !== 'linkscreen1') {
                    clicked($(this)[0], lotId);
                    displayLotInformation(lotId);
                } else if ($.inArray(lotId, g_colAvailable[g_intSelectedModelOptions]) >= 0) {
                    $("#datatable_available_lots tr#" + lotId).trigger('click');
                    g_dataTableLots.rows('#' + lotId).select();
                    var top = $("#panel_lot_available_lots").find(".dataTables_scrollBody").scrollTop() + $('tr#' + lotId).position().top;
                    $("#panel_lot_available_lots").find(".dataTables_scrollBody").animate({scrollTop: top});
                }
            }
        }
    })

    buildSessionOverlay();
}

function displayLotInformation(intLotId) {

    // Fill in current known information about this lot  
    var objLotDetailsContainer = $('#panel_siteplan');
    var objL = g_colLots[intLotId];
    g_intSelectedLot = intLotId;

    $(".panel_siteplan_info").hide();
    $("#panel_siteplan").show();

    showAvailableModels(g_intSelectedLot);

    objLotDetailsContainer.find('tr').css('background-color', PLCOLORS_INFO[objL.size]);
    $('.screen4_details').css('background-color', PLCOLORS_INFO[objL.size]);

    if (!objL.grading) {
        objL.grading = 'N/A';
    }

    if (!objL.closingdate) {
        objL.closingdate = 'N/A';
    }

    objLotDetailsContainer.find('#lot_selected_name').html("Lot " + objL.number.replace(/-/g, ' '));
    objLotDetailsContainer.find('#lot_selected_grading').html('Grading ' + objL.grading);
    objLotDetailsContainer.find('#lot_selected_closingdate').html('Closes on ' + objL.closingdate);

    $('#datatable_available_models tbody').on('click', 'tr', function () {
        g_intSelectedProductLine = this.getAttribute("idpl");
        g_intSelectedModel = this.getAttribute("idc");
        g_intSelectedModelOptions = this.getAttribute("idm");

        $('#datatable_available_models tr').css('background-color', PLCOLORS_INFO[g_intSelectedProductLine]);
        $(this).css('background-color', '#58595b');

        $('#linkscreen3').trigger('click');
        $(".screen3_wrapper_homesites").hide();
    });


    if (!$('.siteplan_table_header th').eq(1).children().length > 0) {
        var theads = $('.siteplan_table_header').find('th');
        theads.eq(1).append(g_IconArea.cloneNode(true));
        theads.eq(2).append(g_IconBath.cloneNode(true));
        theads.eq(3).append(g_IconBed.cloneNode(true));
        theads.eq(4).append(g_IconCoin.cloneNode(true));
    }
}

function showAvailableModels(lotId) {
    var colconnections = g_colLots[lotId].connections;
    var intLotPL = g_colLots[lotId].size;

    var colAvailableModels = g_colModels[intLotPL];


    // Use hashmap to keep track of blocked and available models, faster performance
    var colBlockedModels = {};

    // Fetch all the blocked models related to this lot
    for (var k in colconnections) {
        if (colconnections.hasOwnProperty(k) && g_colAC[k]) {
            var colRules = g_colAC[k].rules;

            for (var l in colRules) {

                if (colRules[l].blockedmodels) {
                    var bm = colRules[l].blockedmodels;
                }

                if (colRules[l].blockedelevations) {
                    var be = colRules[l].blockedelevations;
                }

                if (colRules[l].blockedmodelelevation) {
                    var bme = colRules[l].blockedmodelelevation;
                }

                // For each rule, add this combination or model or elevation to the block list
                // If rule 1 has a combination of given model and elevation, filter out by setting to 0
                for (var k in bme) {
                    if (bme[k] === true) {
                        colBlockedModels[k.replace(/_/g, '-')] = true;
                    }
                }
            }
        }
    }
    //Go through each model and check if this model is in the blocked hashmap
    var colAvailableModelsDatatable = [];
    for (var k in colAvailableModels) {
        if (colAvailableModels.hasOwnProperty(k)) {
            for (var l in colAvailableModels[k]) {

                var objTableM = {};
                var objM = colAvailableModels[k][l];
                var strId = k + '-' + l;
                var baths, beds;

                objTableM.name = objM.model + " " + objM.elevation;

                if (!g_colLots[g_intSelectedLot].premiums || isNaN(g_colLots[g_intSelectedLot].premiums)) {
                    g_colLots[g_intSelectedLot].premiums = 0;
                }

                objTableM.price = addCommas(parseInt(g_colLots[g_intSelectedLot].premiums) + parseInt(objM.price));

                objTableM.code = objM.code;
                objTableM.model = objM.model;
                objTableM.elevation = objM.elevation;
                objTableM.area = objM.area;
                objTableM.size = addCommas(objM.size);

                baths = objM.bathrooms;
                beds = objM.bedrooms;

                if (objM.opt && !isNaN(parseFloat(objM.opt.bathrooms))) {
                    baths += ' - ' + objM.opt.bathrooms;
                }

                if (objM.opt && !isNaN(parseInt(objM.opt.bedrooms))) {
                    beds += ' - ' + objM.opt.bedrooms;
                }

                objTableM.bathrooms = baths;
                objTableM.bedrooms = beds;

                if (!colBlockedModels[strId]) {
                    colAvailableModelsDatatable.push(objTableM);
                }
            }
        }
    }

    g_dataTableModels.clear();
    g_dataTableModels.rows.add(colAvailableModelsDatatable).draw();
}