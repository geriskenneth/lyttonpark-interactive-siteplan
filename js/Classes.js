 /* 
 * These are all the classes defined in Javascript, include these first before using them
 */

// Prefixes to identify imported objects
var g_strLotPrefix = 'Lot_';

// Collections 
var g_colLotSessions = [];
var g_colLotTable;
var g_colAC;

// Firebase database and reference to siteplan svg

  var config = {
    apiKey: "AIzaSyBCqKGKD0RO47baLt1z0tOZLIAArKJE3og",
    authDomain: "medallion-lyttonpark.firebaseapp.com",
    databaseURL: "https://medallion-lyttonpark.firebaseio.com",
    projectId: "medallion-lyttonpark",
    storageBucket: "",
    messagingSenderId: "196111050752",
    appId: "1:196111050752:web:370c966e6022ad86"
  };
  firebase.initializeApp(config);
   
var g_fbdatabase = firebase.database();

firebase.auth().signInAnonymously().catch(function(error) {
  // Handle Errors here.
  var errorCode = error.code;
  var errorMessage = error.message;
  console.log(errorMessage);
});

firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    // User is signed in.
    var isAnonymous = user.isAnonymous;
    var uid = user.uid;
    // console.log(uid);
  } else {
    // User is signed out.
  }
});


// Status needs to be static, only defined once
var STATUS = {
    ID_AVAIL: 1,
    AVAIL: "Available",
    ID_UNAVAILABLE: 2,
    UNAVAILABLE: "Unavailable",
    ID_PENDING: 3,
    PENDING: "Pending",
    ID_SOLD: 4,
    SOLD: "Sold"
}

// RULES needs to be static, only defined once
// TODO: Group rules
var RULES = {
    ID_CUSTOM: 0,
    CUSTOM: "Custom block",
    ID_NEQ_M_E: 1,
    NEQ_M_E: "Not equal model and elevation (2 units rule, opposite)",
    ID_MEQ_B: 2,
    MEQ_B: "Max equal elevation (3 units rule, 10 block row)",
    
    
    /* for in the future to block a whole of elevation or all models*/
    ID_NEQ_M: 3,
    NEQ_M: "Not equal model",
    ID_NEQ_E: 4,
    NEQ_E: "Not equal elevation"
     
}

// Lot class
// Each lot has a list of edges which point to their hash key
function Lot(Number, Size, Premiums, Grading, ClosingDate, Lotconnections) {
    this.number = Number;
    this.size = Size;
    this.premiums = Premiums;
    this.grading = Grading;
    this.closingdate = ClosingDate;
    this.lotconnections = Lotconnections;
}

// Lot controls class for single rules, connection between 2 lots
// Is a bidirectional edge in a graph, connecting two lots, using the object oriented incidence list structure
// Hashmapped by LotOne + LotTwo
// Has an array of rules
// blocked elevations is an array of what the rules have blocked for this lot
// blocked models is an array of what the rules have blocked for this lot
function LotConnection(LotOne, LotTwo, Rules, BlockedE, BlockedM) {
    this.id_lot_1 = LotOne;
    this.id_lot_2 = LotTwo;
    this.rules = Rules;
}

// Model class
/**
 * 
 * @param {type} ArchCode
 * @param {type} Model
 * @param {type} Elevation
 * @param {type} SqFt
 * @param {type} Area
 * @param {type} Bedrooms
 * @param {type} Bathrooms
 * @param {type} Price
 * @returns {Model}
 */
function Model(Area, Bathrooms, Bedrooms, ArchCode, Elevation, Model, Price, SqFt) {
    console.log(this);
    this.area =  Area; 
    this.bathrooms =  Bathrooms;
    this.bedrooms =  Bedrooms;
    this.code = ArchCode;
    this.elevation =  Elevation;
    this.model =  Model;
    this.price =  Price;
    this.size =  SqFt;

}

function ModelOpt(ModelId, Name, SqFt, Price, Bedrooms, Bathrooms) {
    this.model_id = ModelId;
    this.name = Name;
    this.sqft = SqFt;
    this.price = Price;
    this.bedrooms = Bedrooms;
    this.bathrooms = Bathrooms;
 }

// Client class
function Client(Firstname, Lastname, Address, Phone, Email) {
    this.firstname = Firstname;
    this.lastname = Lastname;
    this.address = Address;
    this.phone = Phone;
    this.email = Email;
}

// LotSession
function LotSession(IdLot, IdStatus, IdClient, IdModel, Elevation, CreatedDate){
    this.id_lot = IdLot;
    this.id_status = IdStatus;
    this.id_client = IdClient;
    this.id_model = IdModel;
    this.elevation = Elevation;
    this.created_date = CreatedDate;
}

// Extra functionality to create a timestamp
Object.defineProperty(Date.prototype, 'YYYYMMDDHHMMSS', {
    value: function() {
        function pad2(n) {  // always returns a string
            return (n < 10 ? '0' : '') + n;
        }

        return this.getFullYear() + '-' +
               pad2(this.getMonth() + 1) + '-' +
               pad2(this.getDate()) + ' ' +
               pad2(this.getHours()) + ':' +
               pad2(this.getMinutes()) + ':' +
               pad2(this.getSeconds());
    }
});

// Database savers for each class
// Status is an enum, no class needed
function saveStatus(id, name){
    g_fbdatabase.ref('status/' + id).update({
        name: name
    });
}

function updateLotStatus(lot, status){
    return  g_fbdatabase.ref('lotsessions/' + lot).update({
        id_status: status
    });
}

// Rule is an enum, no class needed
function saveRule(id, description){
    g_fbdatabase.ref('rules/' + id).update({
        description: description
    });
}

function saveLot(objLot){   
    return g_fbdatabase.ref('lots/' + objLot.number).update({
        number: objLot.number,
        size: objLot.size,
        premiums : objLot.premiums,
        grading : objLot.grading,
        closingdate : objLot.closingdate
    });
}

function saveLotTextRotation(lot, rotation){
    return g_fbdatabase.ref('lots/' + lot).update({
        textrotation : rotation
    });
}

function saveLotDotScale(lot, scale){
    return g_fbdatabase.ref('lots/' + lot).update({
        scale : scale
    });
}

function saveLotControls(objLotControls){   
    g_fbdatabase.ref('lotcontrols/' + objLotControls.id_lot).update({
        range: objLotControls.range,
        blocked: objLotControls.blocked
    });
}

function saveStreet(strStreet){    
    
    var ref = g_fbdatabase.ref('streets/');
    var key = ref.push();
    
    return key.set({"key": strStreet});
}
function saveClient(objDummyClient){    
    
    var ref = g_fbdatabase.ref('clients/');
    var key = ref.push();
    
    return key.set({"key": objDummyClient});
}

// Also map everything by area
function saveModel(objModel){
    return g_fbdatabase.ref('models/' + objModel.area + '/' + objModel.code + '/' + objModel.elevation).update({
        area: objModel.area,
        bathrooms: objModel.bathrooms,
        bedrooms: objModel.bedrooms,
        code: objModel.code,
        elevation: objModel.elevation,
        model: objModel.model,
        price:  objModel.price,
        size: objModel.size
    });
}

function saveModelOpt(objOpt){
    
    var m = objOpt.model_id.split('-');    
    var c = m[0] + '-' + m[1];    
    
    if(objOpt.name === 'N/A' || objOpt.name === 'None' ){
        objOpt.name = 'None';
    }
    
    return g_fbdatabase.ref('models/' + m[0] + '/' + c + '/' + m[2] + '/opt/').update({
        name: objOpt.name,
        size: objOpt.sqft,
        price: objOpt.price,
        bedrooms: objOpt.bedrooms,
        bathrooms: objOpt.bathrooms
    });
}

function saveLotSession(objLotSession){
    // Mapped by their corresponding lot
   return  g_fbdatabase.ref('lotsessions/' + objLotSession.id_lot).update({
        id_lot: objLotSession.id_lot,
        id_status: objLotSession.id_status,
        id_client: objLotSession.id_client,
        id_model: objLotSession.id_model,
        elevation: objLotSession.elevation,
        created_date: objLotSession.created_date
    });
}

function removeStreet(keyStreet){
    return g_fbdatabase.ref('streets/').child(keyStreet).remove();
}

function removeLotConnection(objLotConnection){
    if(parseInt(objLotConnection.id_lot_1.split('-')[0]) > parseInt(objLotConnection.id_lot_2.split('-')[0])){
        var tmp = objLotConnection.id_lot_1;
        objLotConnection.id_lot_1 = objLotConnection.id_lot_2;
        objLotConnection.id_lot_2 = tmp;
    }
    
    var strConnectionKey = objLotConnection.id_lot_1 + objLotConnection.id_lot_2;
    
     g_fbdatabase.ref('lotconnections/').child(strConnectionKey).remove();
     
      // Add this edge reference pointer to both nodes  
    var strNode1Connections = 'lots/' + objLotConnection.id_lot_1 + '/connections/';
    var strNode2Connections = 'lots/' + objLotConnection.id_lot_2 + '/connections/';    
    
    g_fbdatabase.ref(strNode1Connections).child(strConnectionKey).remove();    
    g_fbdatabase.ref(strNode2Connections).child(strConnectionKey).remove();
}

function saveLotConnection(objLotConnection){
    
    // hashmap by merging the two nodes
    // put lot 1 and 2 in order from small id to big id, to ensure that there is one edge record between 2 nodes when putting them in a hash table
    
    if(parseInt(objLotConnection.id_lot_1.split('-')[0]) > parseInt(objLotConnection.id_lot_2.split('-')[0])){
        var tmp = objLotConnection.id_lot_1;
        objLotConnection.id_lot_1 = objLotConnection.id_lot_2;
        objLotConnection.id_lot_2 = tmp;
    }
    
    var strNewKey = objLotConnection.id_lot_1 + objLotConnection.id_lot_2;
    
    // set the connection
    g_fbdatabase.ref('lotconnections/' + strNewKey).update({
        id_lot_1: objLotConnection.id_lot_1,
        id_lot_2: objLotConnection.id_lot_2
    });
    
    g_fbdatabase.ref('lotconnections/' + strNewKey + '/rules/').update({
        [objLotConnection.rules]: true
    });
    
    // Add this edge reference pointer to both nodes  
    var strNode1Connections = 'lots/' + objLotConnection.id_lot_1 + '/connections/';
    var strNode2Connections = 'lots/' + objLotConnection.id_lot_2 + '/connections/';    
    
    g_fbdatabase.ref(strNode1Connections).child(strNewKey).update({
        0: true
    });
    
    g_fbdatabase.ref(strNode2Connections).child(strNewKey).update({
        0: true
    }); 
}

/**
 * If given a nodel and elev, will apply block to this combination
 * @param {type} strConnection
 * @param {type} strRule
 * @param {type} strModel
 * @param {type} strElevation
 * @returns {unresolved}
 */
function saveBlockedModelElevation(strConnection, strRule, strModel, strElevation){ 
    
    console.log(strElevation);
    var strConnectionBlockedModels = 'lotconnections/' + strConnection +  '/rules/' + strRule + '/blockedmodelelevation/';  
    
    var update = {};
    update[strModel + '_' + strElevation] = true;
    
    return g_fbdatabase.ref(strConnectionBlockedModels).update(update);    
}

/*
function saveBlockedModel(strConnection, strRule, strModel){    
    var strConnectionBlockedModels = 'lotconnections/' + strConnection +  '/rules/' + strRule + '/blockedmodels/';    
    return g_fbdatabase.ref(strConnectionBlockedModels).child(strModel).update({
        0: true
    });    
}

function saveBlockedElevation(strConnection, strRule, strElevation){    
    var strConnectionBlockedElevation = 'lotconnections/' + strConnection + '/rules/' + strRule + '/blockedelevations/';    
    return g_fbdatabase.ref(strConnectionBlockedElevation).child(strElevation).update({
        0: true
    }); 
}
*/

/**
 * 
 * @param {type} idModel Give an id in the format of 60-2_A
 * @param {type} the lot which has the list of connections
 * @returns {unresolved}
 */ 
function removeModelElevBlock(strRule, idModel, connections){   
    
    console.log(idModel);
    
    var m = idModel.split('_');
    
    var model = m[0];
    var elev = m[1];
    
    var updatedConnections = {};
    
    for(var k in connections){        
        updatedConnections[k + '/rules/' + strRule + '/blockedmodelelevation/' + model + '_' + elev] = false;
    }
    
    console.log(updatedConnections);
    
    var colConnections = g_fbdatabase.ref('lotconnections/');
    colConnections.update(updatedConnections);
}

function getModelsAllPromise(){
    var colModels = g_fbdatabase.ref('models/');
    return colModels.once('value').then(function(snapshot) {
        return snapshot.val();
    }, function(error) {
        console.error(error);
    });
}

function getLotsAllPromise(){
    var colLots = g_fbdatabase.ref('lots/');
    return colLots.once('value').then(function(snapshot) {
        return snapshot.val();
    }, function(error) {
        console.error(error);
    });
}

function getLotSessionsAllPromise(){
    var colLotSessions = g_fbdatabase.ref('lotsessions/');
    return colLotSessions.once('value').then(function(snapshot) {
        return snapshot.val();
    }, function(error) {
        console.error(error);
    });
}

function getLotConnectionsAllPromise(){
    var colLotConnections = g_fbdatabase.ref('lotconnections/');
    return colLotConnections.once('value').then(function(snapshot) {
        return snapshot.val();
    }, function(error) {
        console.error(error);
    });
}

function getStreetsAllPromise(){
    var colStreets = g_fbdatabase.ref('streets/');
    return colStreets.once('value').then(function(snapshot) {
        return snapshot.val();
    }, function(error) {
        console.error(error);
    });
}

function fetchTablesOnce(){
    // Initialize all the current product lines
    // Use Promise to sync all data
    // Fetch both models and lots + lotsessions
    var getModels = getModelsAllPromise();
    var getLots = getLotsAllPromise();
    var getLotSessions = getLotSessionsAllPromise();
    var getLotConnections = getLotConnectionsAllPromise();
    var getStreets = getStreetsAllPromise();
    
    return Promise.all([getModels, getLots, getLotSessions, getLotConnections, getStreets]).then(function(results) {
        return results;
    });
}

