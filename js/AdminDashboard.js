
var g_colModels, g_colLots, g_colLotSessions, g_colAC, g_colStreets;

var tableDataLots, tableDataModels;
var datatableLots, datatableModels;

var tableDataSiting20 = [], tableDataSiting30 = [], tableDataSiting31 = [], tableDataSiting40 = [], tableDataSiting45 = []
var datatableSiting20, datatableSiting30, datatableSiting31, datatableSiting40, datatableSiting45;

var selectedRowLot, selectedRowModel;

var g_strSelectedLot;

var g_colModelsBlocked = {};

var shiftIsPressed, CapsIsPressed;

var selectedPL;

$(document).keydown(function (event) {
    if (event.which == "16") {
        console.log('tgrigger');
        shiftIsPressed = true;
    }

    if (event.which == "18") {
        CapsIsPressed = true;
    }
});

$(document).keyup(function (event) {
    if (event.which == "16") {
        shiftIsPressed = false;
    }

    if (event.which == "18") {
        CapsIsPressed = false;
    }
});

//setupStreetRules();
function setupStreetRules() {

    for (var k in g_colLots) {

        // Split 9-Phase2 into 9 Phase2
        var idnr = g_colLots[k].number.split('-');
        var StartNr = parseInt(idnr[0]);
        var EndNr = parseInt(StartNr + 9);

        var strConnectedId = EndNr + '-' + idnr[1];

        console.log(EndNr, StartNr);
        if (g_colLots[strConnectedId]) {

            var lots = [];

            for (var i = 0; StartNr + i <= EndNr; i++) {
                lots.push(parseInt(StartNr + i) + '-' + idnr[1]);
            }

            var street = new Street(lots);
        }

        console.log(street);

        saveStreet(street);
    }

    g_fbdatabase.ref('streets/').on('value', function (snapshot) {
        console.log(snapshot.val());
    }, function (error) {
        console.error(error);
    });
}

$(document).ready(function () {
    initializeDashboard();
});

function initializeDashboard() {

    $.when(fetchTablesOnce()).then(function (results) {
        // console.log(results);
        g_colModels = results[0];
        g_colLots = results[1];
        g_colLotSessions = results[2];
        g_colAC = results[3];
        g_colStreets = results[4];

        buildModelList();
        buildLotTableData();
        initLotsDataTable();

        buildModelTableData();
        initModelDataTable();

        buildSitingTableData();
        initSitingDataTable();

        var colLots = g_fbdatabase.ref('lots/');
        colLots.on('value', function (snapshot) {
            g_colLots = snapshot.val();
        }, function (error) {
            console.error(error);
        });

        var colSessions = g_fbdatabase.ref('lotsessions/');
        colSessions.on('value', function (snapshot) {
            g_colLotSessions = snapshot.val();
        }, function (error) {
            console.error(error);
        });

        var colModels = g_fbdatabase.ref('models/');
        colModels.on('value', function (snapshot) {
            g_colModels = snapshot.val();
            refreshModels();
        }, function (error) {
            console.error(error);
        });

        var colAC = g_fbdatabase.ref('lotconnections/');
        colAC.on('value', function (snapshot) {
            g_colAC = snapshot.val();
            g_colModelsBlocked = {};
            buildLotBlocks();
        }, function (error) {
            console.error(error);
        });


        var colAC = g_fbdatabase.ref('streets/');
        colAC.on('value', function (snapshot) {
            g_colStreets = snapshot.val();
        }, function (error) {
            console.error(error);
        });

        $('#db_admin_table_lots tbody').on('click', 'tr', function () {
            if ($(this).attr('role') === 'row') {
                $('tr[idl=' + selectedRowLot + ']').removeClass('selected');
                showLotModal(this.getAttribute("idL"));
                selectedRowLot = this.getAttribute("idL");
            }
        });

        $('#db_admin_table_models tbody').on('click', 'tr', function (e) {
            if ($(this).attr('role') === 'row') {
                if (!$(e.target).hasClass('details-control')) {
                    $('tr[idm="'+$(this).data('idm')+'"]').removeClass('selected')
                    // $('tr[idm=' + selectedRowModel + ']').removeClass('selected');
                    showModelModal(this.getAttribute("idm"));
                    selectedRowModel = this.getAttribute("idm");

                } else {
                    var tr = $(this).closest('tr');
                    var row = datatableModels.row(tr);

                    if (row.child.isShown()) {
                        row.child.hide();
                        tr.removeClass('shown');
                    } else {
                        row.child(format(row.data())).show();
                        $('#model_save_opt').on('click', saveModelOptForm);
                        tr.addClass('shown');
                    }
                }
            }
        });

        $('#lot_status').on('change', changeStatus);
        $('#lot_size').on('change', changePL);

        $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {

            if (e.target.id === "tabac") {
                if (!g_blnSiteplanInitialized) {
                    initD3(true);
                }
            }

            if (e.target.id === "tabsiting") {
                buildSitingTableData();
                showSitingTables();
            }
        });

        $('#panel_rules_custom_allowed').on('click', 'span', function () {
            addBlockedModel(g_strSelectedLot, $(this).attr('data-val'));
        });

        $('#panel_rules_custom_blocked').on('click', 'span', function () {
            addAllowModel(RULES.ID_CUSTOM, g_strSelectedLot, $(this).attr('data-val'));
        });

        $('#panel_rules_details').on('click', 'span', function () {
            addAllowModel(RULES.ID_NEQ_M_E, g_strSelectedLot, $(this).attr('data-val'));
        });

        $('#btn_save_rotation').on('click', function () {
            $.when(saveLotTextRotation(g_strSelectedLot, $("#lot_textrotation").val())).then(function (results) {
                buildSiteplanLayout();
            });
        });

        $('#btn_save_scale').on('click', function () {
            $.when(saveLotDotScale(g_strSelectedLot, $("#lot_scale").val())).then(function (results) {
                buildSiteplanLayout();
            });
        });

        /*
         $('#panel_streets_details').on('click', 'span', function(){    
         console.log($(this).attr('data-val'));
         $.when(removeStreet($(this).attr('data-val'))).then(function(results){ 
         showLotAC();
         });
         });
         */
        $('#btn_enable_layout').on('click', function () {
            buildSiteplanLayout();
        });

        $('#btn_enable_display').on('click', function () {
            buildSessionOverlay();
        });

        $('#disable_pl').on('click', 'button', function () {

            $('#modal_confirm').modal('show');
            selectedPL = $(this).attr('data-val');
            $('#confirm_text').html('Disable all ' + selectedPL + "'?");

            $('#save_confirm').off('click');
            $('#save_confirm').on('click', function () {

                $('#modal_confirm').modal('hide');
                for (var k in g_colLotSessions) {
                    if (parseInt(g_colLotSessions[k].id_status) === STATUS.ID_AVAIL
                            && parseInt(g_colLots[k].size) === parseInt(selectedPL)) {
                        updateLotStatus(k, STATUS.ID_UNAVAILABLE);
                    }
                }
            });
        });

        $('#enable_pl').on('click', 'button', function () {
            var pl = $(this).attr('data-val');


            $('#modal_confirm').modal('show');
            selectedPL = $(this).attr('data-val');
            $('#confirm_text').html('Enable all ' + selectedPL + "'?");

            $('#save_confirm').off('click');
            $('#save_confirm').on('click', function () {

                $('#modal_confirm').modal('hide');
                for (var k in g_colLotSessions) {
                    if (parseInt(g_colLotSessions[k].id_status) === STATUS.ID_UNAVAILABLE
                            && parseInt(g_colLots[k].size) === parseInt(selectedPL)) {
                        updateLotStatus(k, STATUS.ID_AVAIL);
                    }
                }
            });
        });
    });
}

function addBlockedModel(lot, idModel) {

    $.when(saveBlockedModelElevation(lot + lot, 0,
            idModel.split('_')[0],
            idModel.split('_')[1])).then(function (results) {

        if ($("a[role='tab'].active").attr('id') === 'tabac') {
            showLotAC();
        }

        if ($("a[role='tab'].active").attr('id') === 'tabsiting') {
            buildSitingTableData();
            showSitingTables();
        }
    });
}

function addAllowModel(rule, lot, idModel) {

    $.when(removeModelElevBlock(rule, idModel, g_colLots[lot].connections)).then(function (results) {
        if ($("a[role='tab'].active").attr('id') === 'tabac') {
            showLotAC();
        }

        if ($("a[role='tab'].active").attr('id') === 'tabsiting') {
            buildSitingTableData();
            showSitingTables();
        }
    });
}


function removeRule(strLotId) {
    if (g_strSelectedLot !== strLotId) {
        console.log('remove' + strLotId);
        var objLotconnection = new LotConnection(g_strSelectedLot, strLotId);
        removeLotConnection(objLotconnection);
        removeLotHighlight(strLotId);
    }
}

function addRule(strLotId) {
    // Rule not equal m and e applied
    // First time, assuming no lots are bought set blocked to empty
    if (g_strSelectedLot !== strLotId) {
        highlightLotAC(strLotId);
        var intRuleNumber = RULES.ID_NEQ_M_E;
        var objLotconnection = new LotConnection(g_strSelectedLot, strLotId, intRuleNumber);
        saveLotConnection(objLotconnection);
        console.log('saved');
    }
}

function removeLotHighlight(strLotId) {
    if (g_colLotTable[strLotId]) {
        var objLotSVG = g_colLotTable[strLotId];
        objLotSVG.setAttribute("fill-opacity", "0.0");
        objLotSVG.style.opacity = 0.0;
    }
}

function initializeAC() {

    for (var i in g_colLotSessions) {
        if (g_colLotSessions.hasOwnProperty(i)) {

            var intLotId = g_colLotSessions[i].id_lot;
            var objLot = g_colLotTable[intLotId];

            objLot.style.opacity = 0.0;
            objLot.setAttribute("fill-opacity", "0");
        }
    }

    g_svg_siteplan.on('click', 'path,polygon,rect', function () {
        if ($(this)[0].id) {

            var lotId = $(this)[0].id.split('_')[1];

            if (!shiftIsPressed && !CapsIsPressed) {
                g_strSelectedLot = lotId;

                var rotation = g_colLots[lotId].textrotation || 0;
                var scale = g_colLots[lotId].scale || 1;

                $('#lot_textrotation').val(rotation);
                $('#lot_scale').val(scale);

                showLotAC();
            }

            if (shiftIsPressed) {
                addRule(lotId);
            }

            if (CapsIsPressed) {
                removeRule(lotId);
            }
        }
    });
}

function highlightLotAC(strLotId) {
    if (g_colLotTable[strLotId]) {
        var objLotSVG = g_colLotTable[strLotId];
        objLotSVG.setAttribute("fill-opacity", "0.5");
        objLotSVG.style.opacity = 1;
        objLotSVG.style.fill = 'cyan';
        objLotSVG.style.stroke = 'blue';
    }
}

function showLotAC() {

    $('#panel_rules_details').empty();

    // Loop for each session, show on the siteplan and add house if sold
    for (var i in g_colLotSessions) {
        if (g_colLotSessions.hasOwnProperty(i)) {

            var intLotId = g_colLotSessions[i].id_lot;
            var objLot = g_colLotTable[intLotId];

            if (intLotId === g_strSelectedLot) {
                objLot.setAttribute("fill-opacity", "0.5");
                objLot.style.opacity = 1;
                objLot.style.fill = 'lime';
                objLot.style.stroke = 'black';
            } else {
                objLot.style.opacity = 0.0;
                objLot.setAttribute("fill-opacity", "0");
            }
        }
    }

    getLotBlocked(g_strSelectedLot);
    showAllowedModels(g_strSelectedLot);

    // console.log(g_colModelsBlocked);

    showStreets(g_strSelectedLot);
}

// Shows the streets that corresponds with this nr
function showStreets(lot) {

    $('#panel_streets_details').empty();

    for (var k in g_colStreets) {

        // Retriever start lot nr and end lot nr from object

        var o = g_colStreets[k].key;
        var phase = o.split(';')[0].split('-')[1];
        var startLotNr = parseInt(o.split(';')[0].split('-')[0]);
        var EndLotNr = parseInt(o.split(';')[1].split('-')[0]);

        var selectedLotNr = parseInt(lot.split('-')[0]);
        var selectedPhase = lot.split('-')[1];

        if (phase === selectedPhase
                && selectedLotNr >= startLotNr
                && selectedLotNr <= EndLotNr) {

            $('#panel_streets_details').append('<br><span data-val="' + k + '" class="minusicon">Part of street ' + startLotNr + ' - ' + EndLotNr + ' ' + phase + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>');
        }
    }
}

function highlightLotStreet(colLots) {

    for (var k in colLots) {
        var objLotSVG = g_colLotTable[k];
        objLotSVG.setAttribute("fill-opacity", "0.5");
        objLotSVG.style.opacity = 1;
        objLotSVG.style.fill = 'magenta';
        objLotSVG.style.stroke = 'magenta';
    }
}

// Shows the list of items in the blocked table
function getLotBlocked(lot) {

    // Loop over all connections on a single lot
    for (var o in g_colLots[lot].connections) {

        // Of any rules are in place
        if (g_colAC[o] && g_colAC[o].rules) {
            for (var k in g_colAC[o].rules) {

                if (g_colAC[o].rules[k].blockedmodelelevation) {
                    var bme = g_colAC[o].rules[k].blockedmodelelevation;
                }

                switch (parseInt(k)) {
                    case RULES.ID_CUSTOM:
                        if (bme) {
                            for (var l in bme) {
                                if (bme[l] === true) {
                                    g_colModelsBlocked[lot][l] = true;
                                    g_colModelsBlocked[lot][l + '_rule'] = 0;
                                }
                            }
                        }
                        break;

                    case RULES.ID_NEQ_M_E:
                        if (bme) {
                            for (var l in bme) {
                                if (bme[l] === true) {


                                    if (g_colAC[o].id_lot_1 !== g_colAC[o].id_lot_2) {
                                        g_colModelsBlocked[lot][l] = true;

                                        if (g_colModelsBlocked[lot][l + '_rule'] !== 0) {
                                            g_colModelsBlocked[lot][l + '_rule'] = 1;
                                        }

                                        var sourceLot = '';


                                        if (g_colAC[o].id_lot_1 !== lot) {
                                            sourceLot = g_colAC[o].id_lot_1;
                                        }

                                        if (g_colAC[o].id_lot_2 !== lot) {
                                            sourceLot = g_colAC[o].id_lot_2;
                                        }

                                        if (g_colModelsBlocked[sourceLot]) {
                                            g_colModelsBlocked[sourceLot][l + '_src'] = lot;
                                        } else {
                                            g_colModelsBlocked[sourceLot] = {};
                                            getLotBlocked(sourceLot);
                                            g_colModelsBlocked[sourceLot][l + '_src'] = lot;
                                        }

                                    } else {
                                        g_colModelsBlocked[lot][l + '_src'] = 'self';
                                    }
                                }
                            }
                        }
                        break;
                }
            }
        }
    }
}

function showAllowedModels(lot) {

    $('#panel_rules_custom_allowed').empty();
    $('#panel_rules_custom_blocked').empty();
    $('#panel_rules_details').empty();

    var pl = g_colModels[g_colLots[g_strSelectedLot].size];
    for (var k in pl) {
        for (var l in pl[k]) {

            var model = pl[k][l].model + ' ' + pl[k][l].elevation;
            var idM = pl[k][l].code + '_' + pl[k][l].elevation;


            if (g_colModelsBlocked[lot][idM]) {


                switch (g_colModelsBlocked[lot][idM + '_rule']) {

                    case RULES.ID_CUSTOM:
                        $('#panel_rules_custom_blocked').append('<span data-val="' + idM + '" class="plusicon"><br>' + model + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>');
                        break;
                    case RULES.ID_NEQ_M_E:
                        $('#panel_rules_details').append('<br>House on ' + g_colModelsBlocked[lot][idM + "_src"] + ' blocks <span data-val="' + idM + '" class="plusicon">' + model + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>');
                        break;
                }

            } else {
                $('#panel_rules_custom_allowed').append('<span data-val="' + idM + '" class="minusicon"><br>' + model + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>');
            }
        }
    }

    for (var o in g_colLots[lot].connections) {

        if (g_colAC[o].id_lot_1 !== lot) {
            highlightLotAC(g_colAC[o].id_lot_1);
        }

        if (g_colAC[o].id_lot_2 !== lot) {
            highlightLotAC(g_colAC[o].id_lot_2);
        }
    }
}

function changePL(e) {
    filterAvailableModels($(this).val());
}

function changeStatus(e) {
    var newStatus = $(this).val();

    switch (parseInt(newStatus)) {
        case STATUS.ID_SOLD:
        case STATUS.ID_PENDING:
            $('#lot_model').prop('disabled', false);
            $('#lot_client').prop('disabled', false);
            $('#lot_client').val('');
            $('#lot_client').attr("placeholder", "Enter name");
            filterAvailableModels($('#lot_size').val());
            break;
        case STATUS.ID_AVAIL:
        case STATUS.ID_UNAVAILABLE:
            $('#lot_model').val('');
            $('#lot_client').val('None');
            $('#lot_model').prop('disabled', true);
            $('#lot_client').prop('disabled', true);
            break;
    }
}

function filterAvailableModels(intPL) {

    $('#lot_model option').hide();
    $('#lot_model option[datapl=' + intPL + ']').show();

    var lot = $("#lot_number").val();

    console.log(lot);
    // Hide those blocked as well
    for (var k in g_colModelsBlocked[lot]) {
        if (g_colModelsBlocked[lot][k] === true) {
            $('#lot_model option[value=' + k.replace(/ /g, "\\ ") + ']').hide();
        }
    }
}

function buildModelList() {
    $("#lot_model").empty();
    $("#lot_model").append('<option value="" selected>None</option>');
    for (var k in g_colModels) {
        for (var l in g_colModels[k]) {
            for (var m in g_colModels[k][l]) {

                var o = g_colModels[k][l][m];
                var value = o.code + '_' + o.elevation;
                var name = o.model + ' ' + o.elevation;
                $("#lot_model").append('<option datapl = "' + o.area + '" value="' + value + '">' + name + '</option>');
            }
        }
    }
}

function buildModelTableData() {

    var arrayRows = [];

    for (var k in g_colModels) {
        for (var l in g_colModels[k]) {
            for (var j in g_colModels[k][l]) {

                var o = g_colModels[k][l][j];
                var a = [];

                a.push(k);
                a.push(l);
                a.push(o.model);
                a.push(o.elevation);
                a.push(o.size);
                a.push(o.price);
                a.push(o.bedrooms);
                a.push(o.bathrooms);

                if (o.opt) {
                    a.push(o.opt.name);
                    a.push(o.opt);
                } else {
                    a.push('None');
                }

                arrayRows.push(a);
            }
        }
    }

    tableDataModels = arrayRows;
}

function buildLotTableData() {
    var intAvail = 0, intSold = 0, intPending = 0, intUnavail = 0;
    tableDataLots = $.map(g_colLots, function (value, index) {


        var arrayRow = [];
        arrayRow.push(value.number);
        arrayRow.push(value.size);
        arrayRow.push(value.premiums);
        arrayRow.push(value.grading || 'None');
        arrayRow.push(value.closingdate || 'None');
        
        var sessionData = g_colLotSessions[index];

        
        // console.log(sessionData.id_status);
        switch (parseInt(sessionData.id_status)) {
            case STATUS.ID_AVAIL:
                intAvail++;
                arrayRow.push(STATUS.AVAIL);
                break;
            case STATUS.ID_SOLD:
                intSold++;
                arrayRow.push(STATUS.SOLD);
                break;
            case STATUS.ID_PENDING:
                intPending++;
                arrayRow.push(STATUS.PENDING);
                break;
            case STATUS.ID_UNAVAILABLE:
                intUnavail++;
                arrayRow.push(STATUS.UNAVAILABLE);
                break;
            default:
                arrayRow.push('Not found');
                break;
        }

        $('#h1avail').html(intAvail);
        $('#h1sold').html(intSold);
        $('#h1pending').html(intPending);
        $('#h1unavail').html(intUnavail);

        // console.log(g_colModels[value.size][sessionData.id_model][sessionData.elevation]);

        if (!sessionData.id_model) {
            arrayRow.push('None');
        } else {
            
            var m = g_colModels[value.size][sessionData.id_model][sessionData.elevation];
            arrayRow.push(m.model + ' ' + m.elevation);
        }

        if (!sessionData.id_client) {
            arrayRow.push('None');
        } else {
            arrayRow.push(sessionData.id_client);
        }

        return [arrayRow];
    });
}

function initModelDataTable() {
    $("#btn_save_model").on('click', saveModelForm);
    datatableModels = $('#db_admin_table_models').DataTable({
        data: tableDataModels,
        render: function (data, type, row) {
            console.log(data);
        },
        columns: [
            {title: "Product Line"},
            {title: "Arch. Code"},
            {title: "Model"},
            {title: "Elev."},
            {title: "Sq. Ft."},
            {title: "Price"},
            {title: "Bedr."},
            {title: "Bathr."},
            {
                "className": 'details-control',
                "orderable": false,
                "title": 'Option (Click to edit)',
                "defaultContent": 'Show option'
            }],
        paging: false,
        oLanguage: {
            "sInfo": "_TOTAL_ entries"},
        select: 'single',
        responsive: true,
        'createdRow': function (row, data, dataIndex) {
            $(row).attr('idM', data[1] + '-' + data[3]);
        }
    });
}

function format(d) {

    // `d` is the original data object for the row
    // Opt is stored in last item of row
    var i = d.length - 1;
    var name = d[i].name || 'N/A';
    var size = d[i].size || 'N/A';
    var price = d[i].price || 'N/A';
    var bathrooms = d[i].bathrooms || 'N/A';
    var bedrooms = d[i].bedrooms || 'N/A';

    return '<table align="right" cellpadding="5" alcellspacing="0" border="0" style="margin-right: 10px;">' +
            '<tr>' +
            '<td>Opt name:</td>' +
            '<td><input type="text" value="' + name + '" id="model_opt_name"></td>' +
            '</tr>' +
            '<tr>' +
            '<td>Square feet:</td>' +
            '<td><input type="text" value="' + size + '" id="model_opt_size"></td>' +
            '</tr>' +
            '<tr>' +
            '<td>Price:</td>' +
            '<td><input type="text" value="' + price + '" id="model_opt_price"></td>' +
            '</tr>' +
            '<tr>' +
            '<td>Bedrooms:</td>' +
            '<td><input type="text" value="' + bedrooms + '" id="model_opt_bedrooms"></td>' +
            '</tr>' +
            '<tr>' +
            '<td>Bathrooms:</td>' +
            '<td><input type="text" value="' + bathrooms + '" id="model_opt_bathrooms"></td>' +
            '</tr>' +
            '<tr>' +
            '<td colspan="2"><button type="button" class="btn btn-primary" datamodel="' + d[1] + '-' + d[3] + '"  id="model_save_opt" >Save option</button></td>' +
            '</tr>' +
            '</table>';
}


function showModelModal(modelId) {

    // console.log(modelId);
    // TODO change index from 30-1 to 30
    var m = modelId.split('-');
    var c = m[0] + '-' + m[1];
    var removeUnderscore = m[2].replace(/_/g, " ");
    var model = g_colModels[m[0]] [c] [removeUnderscore];

    $('#model_pl').val(model.area);
    $('#model_code').val(model.code);
    $('#model_name').val(model.model);
    $('#model_elevation').val(model.elevation);
    $('#model_size').val(model.size);
    $('#model_price').val(model.price);
    $('#model_bedrooms').val(model.bedrooms);
    $('#model_bathrooms').val(model.bathrooms);

    $('#modal_models').modal('show');
}

function initLotsDataTable() {

    // $("#btn_save_lot").on('click', saveLotForm);
    datatableLots = $('#db_admin_table_lots').DataTable({
        data: tableDataLots,
        columns: [
            {title: "Number"},
            {title: "Product line"},
            {title: "Premiums"},
            {title: "Grading"},
            {title: "Closing Date"},
            {title: "Status"},
            {title: "Model"},
            {title: "Client"}],
        paging: false,
        columnDefs: [
            {type: 'any-number', targets: 0}
        ],
        oLanguage: {
            "sInfo": "_TOTAL_ entries"},
        select: 'single',
        responsive: true,
        'createdRow': function (row, data, dataIndex) {
            $(row).attr('idL', data[0]);
        }
    });
}

function buildLotBlocks() {
    // Build hashmap and add existing blocks to the table
    for (var k in g_colLots) {
        var lot = g_colLots[k];
        var lotId = lot.number;

        if (!g_colModelsBlocked[lotId]) {
            g_colModelsBlocked[lotId] = {};
            getLotBlocked(lotId);
        }
    }
}

function buildSitingTableData() {

    tableDataSiting20 = [];
    tableDataSiting30 = [];
    tableDataSiting31 = [];
    tableDataSiting40 = [];
    tableDataSiting45 = [];
    buildLotBlocks();

    //Build array based on existing blocks, if it doesn't exist then add no value as filler    
    for (var k in g_colModelsBlocked) {

        var rowData = [];
        var pl = g_colLots[k].size;

        rowData.push(k);

        for (var l in g_colModels[pl]) {
            for (var m in g_colModels[pl][l]) {

                var o = g_colModels[pl][l][m];
                var id = o.code + '_' + o.elevation;

                if (g_colModelsBlocked[k][id] === true) {
                    // console.log('push N');
                    rowData.push('N');
                } else {
                    rowData.push('Y');
                }
            }


        }

        switch (parseInt(pl)) {
            case 20:
                tableDataSiting20.push(rowData);
                break;
            case 30:
                tableDataSiting30.push(rowData);
                break;
            case 31:
                tableDataSiting31.push(rowData);
                break;                
            case 40:
                tableDataSiting40.push(rowData);
                break;
            case 45:
                tableDataSiting45.push(rowData);
                break;                
        }
    }
}

function showSitingTables() {
    datatableSiting20.clear();
    datatableSiting20.rows.add(tableDataSiting20);
    datatableSiting20.draw();

    datatableSiting30.clear();
    datatableSiting30.rows.add(tableDataSiting30);
    datatableSiting30.draw();

    datatableSiting31.clear();
    datatableSiting31.rows.add(tableDataSiting31);
    datatableSiting31.draw();

    datatableSiting40.clear();
    datatableSiting40.rows.add(tableDataSiting40);
    datatableSiting40.draw();

    datatableSiting45.clear();
    datatableSiting45.rows.add(tableDataSiting45);
    datatableSiting45.draw();    
}

_anyNumberSort = function (a, b, high) {
    var reg = /[+-]?((\d+(\.\d*)?)|\.\d+)([eE][+-]?[0-9]+)?/;
    a = a.replace(',', '.').match(reg);
    a = a !== null ? parseFloat(a[0]) : high;
    b = b.replace(',', '.').match(reg);
    b = b !== null ? parseFloat(b[0]) : high;
    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
}

jQuery.extend(jQuery.fn.dataTableExt.oSort, {
    "any-number-asc": function (a, b) {
        return _anyNumberSort(a, b, Number.POSITIVE_INFINITY);
    },
    "any-number-desc": function (a, b) {
        return _anyNumberSort(a, b, Number.NEGATIVE_INFINITY) * -1;
    }
});

function initSitingDataTable() {

    // Build the table headers based on what's available
    // Table headers will be built based on what's inside the database
    for (var k in g_colModels) {
        for (var l in g_colModels[k]) {

            var countColSpan = 0;
            var elevsColumns = '';

            for (var i in g_colModels[k][l]) {

                countColSpan++;
                var o = g_colModels[k][l][i];

                elevsColumns += '<th>' + o.elevation + '</th>';

                // console.log(o.elevation);
            }

            var theadModel;
            var theadElev;

            switch (parseInt(k)) {
                case 20:
                    theadModel = $('#db_admin_table_siting_20 .thead_models');
                    theadElev = $('#db_admin_table_siting_20 .thead_elevs');
                    break;                
                case 30:
                    theadModel = $('#db_admin_table_siting_30 .thead_models');
                    theadElev = $('#db_admin_table_siting_30 .thead_elevs');
                    break;
                case 31:
                    theadModel = $('#db_admin_table_siting_31 .thead_models');
                    theadElev = $('#db_admin_table_siting_31 .thead_elevs');
                    break;                        
                case 40:
                    theadModel = $('#db_admin_table_siting_40 .thead_models');
                    theadElev = $('#db_admin_table_siting_40 .thead_elevs');
                    break;
                case 45:
                    theadModel = $('#db_admin_table_siting_45 .thead_models');
                    theadElev = $('#db_admin_table_siting_45 .thead_elevs');
                    break;                    
            }
// console.log(g_colModels[k][l]);
            if(g_colModels[k][l]['A'])
                theadModel.append('<th colspan="' + countColSpan + '">' + g_colModels[k][l]['A'].code + '</th>');
            else if( g_colModels[k][l]['A1'])
                theadModel.append('<th colspan="' + countColSpan + '">' + g_colModels[k][l]['A1'].code + '</th>');

            theadElev.append(elevsColumns);
        }
    }

    /* Create an array with the values of all the input boxes in a column, parsed as numbers */
    $.fn.dataTable.ext.order['dom-text-numeric'] = function (settings, col) {
        return this.api().column(col, {order: 'index'}).nodes().map(function (td, i) {
            console.log(td);
            return 1;
        });
    };

    datatableSiting20 = $('#db_admin_table_siting_20').DataTable({
        data: tableDataSiting20,
        paging: false,
        columnDefs: [
            {type: 'any-number', targets: 0}
        ],
        oLanguage: {
            "sInfo": "_TOTAL_ entries"},
        responsive: true,
        'createdRow': function (row, data, dataIndex) {
            //console.log(row, data, dataIndex);            
            buildSitingRow(row, data, dataIndex);
        }
    });
    $('#db_admin_table_siting_20').on('click', 'td', toggleSitingBlock);

    datatableSiting30 = $('#db_admin_table_siting_30').DataTable({
        data: tableDataSiting30,
        paging: false,
        columnDefs: [
            {type: 'any-number', targets: 0}
        ],
        oLanguage: {
            "sInfo": "_TOTAL_ entries"},
        responsive: true,
        'createdRow': function (row, data, dataIndex) {
            buildSitingRow(row, data, dataIndex);
        }
    });
    $('#db_admin_table_siting_30').on('click', 'td', toggleSitingBlock);

    datatableSiting31 = $('#db_admin_table_siting_31').DataTable({
        data: tableDataSiting31,
        paging: false,
        columnDefs: [
            {type: 'any-number', targets: 0}
        ],
        oLanguage: {
            "sInfo": "_TOTAL_ entries"},
        responsive: true,
        'createdRow': function (row, data, dataIndex) {
            buildSitingRow(row, data, dataIndex);
        }
    });
    $('#db_admin_table_siting_31').on('click', 'td', toggleSitingBlock);

    datatableSiting40 = $('#db_admin_table_siting_40').DataTable({
        data: tableDataSiting40,
        paging: false,
        columnDefs: [
            {type: 'any-number', targets: 0}
        ],
        oLanguage: {
            "sInfo": "_TOTAL_ entries"},
        responsive: true,
        'createdRow': function (row, data, dataIndex) {
            buildSitingRow(row, data, dataIndex);
        }
    });
    $('#db_admin_table_siting_40').on('click', 'td', toggleSitingBlock);

    datatableSiting45 = $('#db_admin_table_siting_45').DataTable({
        data: tableDataSiting45,
        paging: false,
        columnDefs: [
            {type: 'any-number', targets: 0}
        ],
        oLanguage: {
            "sInfo": "_TOTAL_ entries"},
        responsive: true,
        'createdRow': function (row, data, dataIndex) {
            buildSitingRow(row, data, dataIndex);
        }
    });
    $('#db_admin_table_siting_45').on('click', 'td', toggleSitingBlock);    
    
    /* Create an array with the values of all the input boxes in a column, parsed as numbers */
    $.fn.dataTable.ext.order['dom-text-numeric'] = function (settings, col) {
        return this.api().column(col, {order: 'index'}).nodes().map(function (td, i) {
            return $('input', td).val() * 1;
        });
    }
}

function toggleSitingBlock() {

    var tdI = $(this).index();
    var elev = $(this).closest('table').find('.thead_elevs th').eq(tdI).text();

    var models = $(this).closest('table').find('.thead_models th');

    var model;
    var colspanTotal = 0, colspanPrevious = 0;

    // Match clicked td index with header of models by counting colspans
    models.each(function (index) {
        colspanPrevious = colspanTotal;
        colspanTotal += parseInt($(this).attr('colspan'));

        console.log(index, colspanPrevious, colspanTotal);
        if (tdI > 0 && tdI >= colspanPrevious && tdI < colspanTotal) {
            model = $(this).text();
            return false;
        }
    });

    var lot = $(this).closest('tr').attr('id');

    console.log('Block ' + lot, model + '_' + elev);
    if (tdI > 0) {
        if ($(this).text() === 'Y') {
            addBlockedModel(lot, model + '_' + elev);
        } else {
            addAllowModel(RULES.ID_CUSTOM, lot, model + '_' + elev);
        }
    } else {
        //TODO buttons
    }
}

function buildSitingRow(row, data, dataIndex) {
    $(row).attr('id', data[0]);

    var text = $('td', row).eq(0).text();

    /*
     $('td', row).eq(0).append('&nbsp;&nbsp;');    
     $('td', row).eq(0).append('<button class=".btn plusicon" >Allow all &nbsp;&nbsp;&nbsp</button>');
     $('td', row).eq(0).append('<button class=".btn minusicon" >Block all &nbsp;&nbsp;&nbsp;</button>');
     */

    for (var i = 0; i < data.length; i++) {
        var td = $('td', row).eq(i);

        if (data[i] === 'N') {
            td.addClass('highlightN');
        } else if (data[i] === 'Y') {
            td.addClass('highlightY');
        }
    }
}


function showLotModal(lotId) {

    var lot = g_colLots[lotId];
    var ses = g_colLotSessions[lotId];

    $('#lot_number').val(lot.number);
    $('#lot_size').val(lot.size);
    $('#lot_premiums').val(lot.premiums);
    $('#lot_grading').val(lot.grading);
    $('#lot_closingdate').val(lot.closingdate);
    $('#lot_status').val(ses.id_status);

    if (ses.id_status != STATUS.ID_SOLD) {
        $('#lot_model').prop('disabled', true);
        $('#lot_client').prop('disabled', true);
        $('#lot_model').val('');
        
        if ($('#lot_premiums').attr('disabled') == 'disabled' && parseInt(ses.id_status) === STATUS.ID_UNAVAILABLE) {
            $('#btn_save_lot').hide();
            $('#lot_status').attr('disabled', true);
        }else{
            $('#btn_save_lot').show();
            $('#lot_status').attr('disabled', false);
        }

    } else {
        filterAvailableModels(lot.size);
        $('#lot_model').prop('disabled', false);
        $('#lot_client').prop('disabled', false);
        $('#lot_model').val(ses.id_model + '_' + ses.elevation);
    }

    if (ses.id_client === '0') {
        ses.id_client = 'None';
    }

    $('#lot_client').val(ses.id_client);

    $('#modal_lots').modal('show');
}

$('#btn_save_lot').click(function(e) {
    e.preventDefault();
    
    //status value
    var selected_value = $('#lot_model').val();

    // Lot
    var lot = $('#lot_number').val(),
            size = $('#lot_size').val(),
            premiums = $('#lot_premiums').val(),
            grading = $('#lot_grading').val(),
            closingdate = $('#lot_closingdate').val(),
            connections = g_colLots[lot].connections;

    // Session
    var status = $('#lot_status').val(),
            client = $('#lot_client').val();

    var strModel = '';
    var strElev = '';
    if ($('#lot_model').val()) {
        strModel = $('#lot_model').val().split('_')[0];
        strElev = $('#lot_model').val().split('_')[1];
    }

    var model = strModel;
    var elevation = strElev;
    var createdDate = new Date().YYYYMMDDHHMMSS();

    var newLot = new Lot(lot, size, premiums, grading, closingdate, connections);
    var newSession = new LotSession(lot, status, client, model, elevation, createdDate);

    // When saving the lotsession, include the add the blocked models and elevations to the lot conncetions
    if (parseInt(status) === STATUS.ID_PENDING || parseInt(status) === STATUS.ID_SOLD) {
        if(!selected_value){
            window.alert('Please select a model');
            return false;
        }

        // Release old rules just in case
        releaseBlockingRules(lot);        
        applyBlockingRules(lot, strModel, strElev);        
    }

    // Free up the lot rules in case it gets set to available again
    if (parseInt(status) === STATUS.ID_AVAIL) {
        releaseBlockingRules(lot);     
    }

    $.when(saveLot(newLot), saveLotSession(newSession)).then(function (results) {
        buildLotTableData();
        datatableLots.clear();
        datatableLots.rows.add(tableDataLots);
        datatableLots.draw();
        $('tr[idl=' + selectedRowLot + ']').addClass('selected');
        $('#modal_lots').modal('hide');
    });
})

function releaseBlockingRules(lot){
    for (var k in g_colModelsBlocked[lot]) {

         var model = g_colLotSessions[lot].id_model + '_' + g_colLotSessions[lot].elevation;
         var src = g_colModelsBlocked[lot][k + '_src'];

         console.log(model, lot);
         if (g_colModelsBlocked[lot][k + '_rule'] === RULES.ID_NEQ_M_E
                 && k === model) {

             addAllowModel(RULES.ID_NEQ_M_E, lot, k);

             if (g_colLotSessions[lot].elevation.toUpperCase() == 'A') {
                 addAllowModel(RULES.ID_NEQ_M_E, lot, k.replace("A", "D"));
             }

             if (g_colLotSessions[lot].elevation.toUpperCase() == 'D') {
                 addAllowModel(RULES.ID_NEQ_M_E, lot, k.replace("D", "A"));
             }
         }
     }
}

function applyBlockingRules(lot, model, elevation) {
    var colconnections = g_colLots[lot].connections;

    //AC individual rules
    for (var k in colconnections) {
        if (colconnections.hasOwnProperty(k) && g_colAC[k]) {

            var colRules = g_colAC[k].rules;

            // For each rule, apply the blocking rule by adding the model / elevations to the list
            for (var l in colRules) {
                switch (parseInt(l)) {
                    case RULES.ID_NEQ_M_E:
                        saveBlockedModelElevation(k, l, model, elevation);

                        // Special woodhaven rules, when blocking A also block D
                        var m = model.split('-');
                        if (elevation.toUpperCase() == 'A' && g_colModels[m[0]][model][elevation]) {
                            saveBlockedModelElevation(k, l, model, 'D');
                        }

                        if (elevation.toUpperCase() == 'D' && g_colModels[m[0]][model][elevation]) {
                            saveBlockedModelElevation(k, l, model, 'A');
                        }

                        break;
                }
            }
        }
    }

    var selectedLotNr = parseInt(lot.split('-')[0]);
    var selectedPhase = lot.split('-')[1];

    //Street rules
    for (var k in g_colStreets) {
        var o = g_colStreets[k].key;
        var phase = o.split(';')[0].split('-')[1];
        var startLotNr = parseInt(o.split(';')[0].split('-')[0]);
        var EndLotNr = parseInt(o.split(';')[1].split('-')[0]);

        if (phase === selectedPhase
                && selectedLotNr >= startLotNr
                && selectedLotNr <= EndLotNr) {

            for (var i = startLotNr; i <= EndLotNr; i++) {

                // max 3 models next to each other
                if (check3ModelsNeighbours(selectedPhase, i, model)) {
                    console.log("3 models next to each other detected");
                }

            }
        }
    }
}

function check3ModelsNeighbours(Phase, lotNr, model) {


    if (g_colLotSessions[(lotNr - 1) + '-' + Phase]
            && g_colLotSessions[(lotNr + 1) + '-' + Phase]) {

        var l1 = g_colLotSessions[(lotNr - 1) + '-' + Phase];
        var s1 = l1.id_status;

        var l2 = g_colLotSessions[(lotNr + 1) + '-' + Phase];
        var s2 = l2.id_status;

        console.log("1:" + l1.id_model, "2:" + l2.id_model, "3:" + model);
        if ((s1 == STATUS.ID_SOLD || s1 == STATUS.ID_PENDING)
                && (s2 == STATUS.ID_SOLD || s2 == STATUS.ID_PENDING)) {

            if (l1.id_model === l2.id_model && l2.id_model === model) {
                return true;
            }
        } else {
            return false;
        }

    } else {
        return false;
    }
}

function saveModelForm() {
    var area = $('#model_pl').val(),
            code = $('#model_code').val(),
            model = $('#model_name').val(),
            elevation = $('#model_elevation').val(),
            sqft = $('#model_size').val(),
            price = $('#model_price').val(),
            bathrooms = $('#model_bathrooms').val(),
            bedrooms = $('#model_bedrooms').val();
            //ORDER Area, Bathrooms, Bedrooms, ArchCode, Elevation, Model, Options, Highlight, Price, SqFt

    var newModel = new Model(area, bathrooms, bedrooms, code, elevation, model, price, sqft);
    saveModel(newModel);
}

function saveModelOptForm(e) {


    var model_id = $(e.target).attr('datamodel');

    var name = $('#model_opt_name').val(),
            sqft = $('#model_opt_size').val(),
            price = $('#model_opt_price').val(),
            bedrooms = $('#model_opt_bedrooms').val(),
            bathrooms = $('#model_opt_bathrooms').val();

    var newModelOpt = new ModelOpt(model_id, name, sqft, price, bedrooms, bathrooms);
    saveModelOpt(newModelOpt);
}

function refreshModels() {
    buildModelTableData();
    datatableModels.clear();
    datatableModels.rows.add(tableDataModels);
    datatableModels.draw();

    if (selectedRowModel) {
        $('tr[idm=' + selectedRowModel.replace(/ /g, "\\ ") + ']').addClass('selected');
    }

    $('#modal_models').modal('hide');
}