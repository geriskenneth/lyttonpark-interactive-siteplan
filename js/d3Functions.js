/*
 * All stuff related to d3 functionality goes in here
 */

var width, height, path, g, svg, scale;
var zoom, prevX, prevY;

var g_blnSiteplanInitialized = false;
var g_svg_siteplan;
    
function initD3(blnAdmin){
    
    // Use d3 to convert external svg to inline svg
    d3.xml("siteplan.svg").mimeType("image/svg+xml").get(function(error, xml) {
        if (error) throw error;
        document.getElementById('container_siteplan_svg').appendChild(xml.documentElement);
    });

    var target = document.getElementById('container_siteplan_svg');
    var observer = new MutationObserver(function(mutations) {
      mutations.forEach(function(mutation) {

        if(mutation.type === 'childList'){

            if(!g_blnSiteplanInitialized){

                g_blnSiteplanInitialized = true;

                // Set the SVG right
                g_svg_siteplan = $('#siteplan_svg');   
                g_colLotTable = g_svg_siteplan.find("path").toArray().reduce(function(map, obj) {
                    map[obj.id.split('_')[1]] = obj;
                    return map;
                }, {}); 
                
                g_svg_siteplan.find("polygon").toArray().reduce(function(map, obj) {
                    g_colLotTable[obj.id.split('_')[1]] = obj;
                }, {}); 
                
                //console.log(g_colLotTable);

                width = $('#siteplan_svg').width();
                height = $('#siteplan_svg').height();

                path = d3.geoPath()
                    .projection(matrix(1, 0, 0, -1, 0, height)); 

                svg = d3.select("#siteplan_svg")
                    .attr("width", width)
                    .attr("height", height)
                    .on("click", stopped, true);
                scale = 1;

                zoom = d3.zoom()
                    .scaleExtent([1, 5])
                    .translateExtent([[0,0] , [width, height]])
                    .on("zoom", zoomed);

                g = d3.select("#siteplan_g");  
                svg.on("touchstart", touchStart)
                    .on("touchmove", touchStatus);

                if(!blnAdmin) {
                    initializeSitePlan();
                }else{
                    svg.call(zoom);
                    initializeAC();
                }
                /**zoom into section on load */
                svg.transition().call( zoom.transform, d3.zoomIdentity.translate(0, 0).scale(1));
                // In case of importing a new map, enable this function to create the new database
                // setupSitePlan();    
            }

            observer.disconnect();
        }    
      });    
    });
    
        // configuration of the observer:
    var config = { attributes: true, childList: true, characterData: true, subtree: true};
    observer.observe(target, config);
}

function touchStart(){
    prevX = d3.event.changedTouches[0].clientX;
    prevY = d3.event.changedTouches[0].clientY;
}

function touchStatus(){
    d3.event.preventDefault();
    var x = (d3.event.changedTouches[0].clientX - prevX);
    var y = (d3.event.changedTouches[0].clientY - prevY);
    
    prevX = d3.event.changedTouches[0].clientX;
    prevY = d3.event.changedTouches[0].clientY;    
    
    svg.call(zoom.translateBy, x/scale, y/scale);
}
    
function clearPulseSVG(){
    $.each(g_colLotTable, function( index, value ) {
        $(this).removeClass('pulse-svg');
    });
}

function pulseSVG(d){
    
    var pl = g_colLots[d.id.split('_')[1]].size;
    // d.style.fill = PLCOLORS_INFO[pl];
    d.style.fill = "green";
    d.style.stroke = PLCOLORS_INFO[pl];
    d.style.fillOpacity = 1;
    var lotID = $(d).attr('id');
    
    $('#'+lotID).addClass("pulse-svg");
}

function clicked(d, lotId) {      
    var bounds = d.getBBox();
    

    clearPulseSVG();
    pulseSVG(d);
    g_svgSelectedLot = $(d);
    
    var dx = bounds.width;
    var dy = bounds.height;    
    
    var x =  bounds.x + (dx/2);
    var y =  bounds.y + (dy/2);
    
    //console.log(x, y);
    scale = 5;
    
    var maxX = Math.min(0, Math.max(width / 2 - scale * x, width - width * scale));
    var maxY = Math.min(0, Math.max(height / 2 - scale * y, height - height * scale));    
         
    svg.transition()        
      .duration(2000)
      .call( zoom.transform, d3.zoomIdentity.translate(maxX,maxY).scale(scale));

      if(scale > 3){
          $('.keymap').fadeOut()
      }else{
        $('.keymap').fadeIn()
      }
}

function reset() {
    svg.transition().call( zoom.transform, d3.zoomIdentity.translate(0, 0).scale(1));
    $('.keymap').fadeIn()
  svg.transition()
      .duration(2000)
      .call( zoom.transform, d3.zoomIdentity ); // updated for d3 v4
}

function zoomed() {   
    
    //If zoomed out and buildhomesites is selected
  if(scale === 1 && scale !== parseInt(d3.event.transform.k) 
    && $('a.activeMenu').length && $('a.activeMenu').get(0).id === 'linkscreen1'){

        //console.log('trigger pulse group');
        var colOptions = g_colAvailable[g_intSelectedModelOptions];  
       
        for(var k in colOptions){            
            if(colOptions[k] !==0){
                pulseSVG(g_colLotTable[colOptions[k]]);
            }
        }        
  }

  g.attr("transform", d3.event.transform); // updated for d3 v4  
  scale = d3.event.transform.k;   
  
  if($("#zoomslider").length){
        $("#zoomslider").slider('setValue', scale, false);
  }
}

// If the drag behavior prevents the default click,
// also stop propagation so we don’t click-to-zoom.
function stopped() {
  if (d3.event.defaultPrevented) d3.event.stopPropagation();
}

// To project a flat svg in d3
// https://en.wikipedia.org/wiki/Transformation_matrix#Affine_transformations
function matrix(a, b, c, d, tx, ty) {
  return d3.geoTransform({
    point: function(x, y) {
      this.stream.point(a * x + b * y + tx, c * x + d * y + ty);
    }
  });
}