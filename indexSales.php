<?php
session_start();
$validUser = $_SESSION["login"] === true;

if(!$validUser) {
    // header("Location: http://localhost:8888/login.php"); die();
    header("Location: http://guidelineshosting.com/lyttonpark-kiosk/login.php"); die();

}

?>


<!DOCTYPE html>
<html>
<head>
    <title>SitePlan - Administrative use</title>
    <link rel="stylesheet" href="lib/bootstrap-4.0.0-beta/css/bootstrap.min.css"> 
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.2/css/select.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.0/css/responsive.bootstrap4.min.css">
    
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">   
    <link rel="stylesheet" type="text/css" href="lib/datepicker/bootstrap-datepicker.min.css">   
    
    <link rel="stylesheet" type="text/css" href="css/admindashboard.css">
</head>
<body>
<nav class="navbar fixed-top navbar-expand-md navbar-dark bg-primary mb-3" style="justify-content: space-between;">    
    <a class="navbar-brand" href="#" title="Free Bootstrap 4 Admin Template">LyttonPark sales access</a>    
    <a class=text-right" style="color: white;" href="logout.php" title="Logout">Logout</a>
</nav>
<div class="container-fluid" id="main">
    <div class="row row-offcanvas row-offcanvas-left">
        <div class="col-md-12 col-lg-12 main">

            <div class="tab-content">
                <div id="db_tab_content_lots" class="tab-pane fade show active" role="tabpanel">

                    <h1 class="display-4 d-none d-sm-block">
                        Overview of lots
                    </h1>

                    <div class="row mb-3">
                        <div class="col-xl-3 col-sm-6">
                            <div class="card bg-info text-white h-100">
                                <div class="card-body bg-info">
                                    <div class="rotate">
                                        <i class="fa fa-home fa-3x"></i>    
                                    </div>
                                    <h6 class="text-uppercase">Available</h6>
                                    <h1 class="display-4" id='h1avail'></h1>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-sm-6">
                            <div class="card text-white bg-danger h-100">
                                <div class="card-body bg-danger">
                                    <div class="rotate">
                                        <i class="fa fa-usd fa-3x"></i>
                                    </div>
                                    <h6 class="text-uppercase">Sold</h6>
                                    <h1 class="display-4" id='h1sold'></h1>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-sm-6">
                            <div class="card text-white bg-zqrning h-100">
                                <div class="card-body bg-warning">
                                    <div class="rotate">
                                        <i class="fa fa-hourglass fa-3x"></i>
                                    </div>
                                    <h6 class="text-uppercase">Pending</h6>
                                    <h1 class="display-4" id='h1pending'></h1>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-sm-6">
                            <div class="card text-white bg-success h-100">
                                <div class="card-body bg-success">
                                    <div class="rotate">
                                        <i class="fa fa-ban fa-3x"></i>
                                    </div>
                                    <h6 class="text-uppercase">Unavailable</h6>
                                    <h1 class="display-4" id='h1unavail'></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->
                    <hr>
            
                    <h3>Table of all lots</h3>
                    <table id="db_admin_table_lots" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead class="thead-inverse"></thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
    
<div id="modal_lots" class="modal fade">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit lot</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="form-group row">
              <label  class="col-2 col-form-label">Number</label>
              <div class="col-10">
                <input class="form-control" type="text" value="" id="lot_number" disabled>
              </div>
            </div>
          
            <div class="form-group row">
              <label  class="col-2 col-form-label">Product line</label>
              <div class="col-10">
                  <select class="form-control" id="lot_size" disabled>
                  <option value="20">20</option>
                  <option value="30">30 Rear Lane</option>
                  <option value="31">30</option>
                  <option value="40">40</option>
                  <option value="45">45</option>
                  </select>
              </div>
            </div>
          
            <div class="form-group row">
              <label  class="col-2 col-form-label" >Premiums</label>
              <div class="col-10">
                <input class="form-control" type="number" value="" id="lot_premiums" disabled="true">
              </div>
            </div>
          
            <div class="form-group row">
              <label  class="col-2 col-form-label">Grading</label>
              <div class="col-10">
                <input class="form-control" type="text" value="" id="lot_grading" disabled>
              </div>
            </div>
          
            <div class="form-group row">
              <label  class="col-2 col-form-label">Closing Date</label>
              <div class="col-10">
                <input class="form-control" type="text" value="" id="lot_closingdate" disabled>
              </div>
            </div>
          
            <div class="form-group row">
              <label  class="col-2 col-form-label">Status</label>
              <div class="col-10">
                <select class="form-control" id="lot_status">
                        <option value="1">Available</option>
                        <option value="4">Sold</option>
                  </select>
              </div>
            </div>
          
            <div class="form-group row">
              <label  class="col-2 col-form-label">Model</label>
              <div class="col-10">
                  <select class="form-control" id="lot_model">
                      
                  </select>
              </div>
            </div>
          
            <div class="form-group row">
              <label  class="col-2 col-form-label">Client</label>
              <div class="col-10">
                <input class="form-control" type="text" value="" id="lot_client">
              </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btn_save_lot">Save changes</button>
      </div>
    </div>
  </div>
</div>
    
<script src="lib/firebase/firebase.js"></script>
<script src="lib/firebase/firebase-app.js"></script>
<script src="lib/firebase/firebase-auth.js"></script>
<script src="lib/firebase/firebase-database.js"></script>
<script src="lib/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>

<script src="lib/d3/d3.min.js" type="text/javascript"></script>
<script src="lib/popper/popper.min.js"></script>
<script src="lib/bootstrap-4.0.0-beta/js/bootstrap.min.js"></script>

<script src="lib/datatables/jquery.dataTables.min.js"></script>
<script src="lib/datatables/dataTables.bootstrap4.min.js"></script>
<script src="lib/datatables/dataTables.select.min.js"></script>
<script src="lib/datatables/dataTables.responsive.min.js"></script> 

<script src="lib/datepicker/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="js/FunctionsSitePlan.js" type="text/javascript"></script>
<script src="js/d3Functions.js" type="text/javascript"></script>

<script src="js/Classes.js" type="text/javascript"></script>
<script src="js/AdminDashboard.js" type="text/javascript"></script>

</body>
</html>