#Interactive Siteplan with architectural controls*

The site plan tables also maintain architectural control, ensuring that home designs are not repeated, in order to preserve the integrity of the streetscape. And when the buyer is feeling comfortable and the decision to purchase is made, a sales representation uses an iPad to input the information into the system.
This eliminates selling the same lot twice.

#View Mode
1920x1080 fullscreen touch mode

#Admin Panel
http://kennethgeris.com/lyttonpark-kiosk/indexAdmin.php
id: admin, pw: adminlyttonpark

#Sales rep Panel
http://kennethgeris.com/lyttonpark-kiosk/indexSales.php
id: admin, pw: saleslyttonpark

#Touchscreen
http://kennethgeris.com/lyttonpark-kiosk/

Init: Click on the community tab and select a random lot to initialize the siteplan functionality.