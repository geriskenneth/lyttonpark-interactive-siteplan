<?php
session_start();
$validUser = $_SESSION["login"] === true;
$admin = $_SESSION["admin"] === true;

if (!$validUser || !$admin) {
    header("Location: http://localhost:8888/login.php");
    // header("Location: http://guidelineshosting.com/lyttonpark-kiosk/login.php"); die();

    die();
}
?>


<!DOCTYPE html>
<html>
    <head>
        <title>SitePlan - Administrative use</title>
        <link rel="stylesheet" href="lib/bootstrap-4.0.0-beta/css/bootstrap.min.css"> 
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.2/css/select.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.0/css/responsive.bootstrap4.min.css">

        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">   
        <link rel="stylesheet" type="text/css" href="lib/datepicker/bootstrap-datepicker.min.css">   

        <link rel="stylesheet" type="text/css" href="css/admindashboard.css">
    </head>
    <body>

        <nav class="navbar fixed-top navbar-expand-md navbar-dark bg-primary mb-3" style="justify-content: space-between;">    
            <a class="navbar-brand" href="#" title="Free Bootstrap 4 Admin Template">LyttonPark administration access</a>
            <a class=text-right" style="color: white;" href="logout.php" title="Logout">Logout</a>
        </nav>
        <div class="container-fluid" id="main">
            <div class="row row-offcanvas row-offcanvas-left">
                <div class="col-md-3 col-lg-2 sidebar-offcanvas" id="sidebar" role="navigation">

                    
                    <ul class="nav flex-column pl-1" role="tablist">
                        <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#db_tab_content_lots">Overview lots</a></li>
                        <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#db_tab_content_models">Overview Models</a></li>
                        <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" id="tabac" href="#db_tab_content_ac">Architectual controls & Site Plan layout</a></li>
                        <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" id="tabsiting" href="#db_tab_content_siting">Siting table</a></li>
                    </ul>
                </div>
                <!--/col-->

                <div class="col-md-9 col-lg-10 main">

                    <div class="tab-content">
                        <div id="db_tab_content_lots" class="tab-pane fade show active" role="tabpanel">

                            <h1 class="display-4 d-none d-sm-block">
                                Overview of lots
                            </h1>

                            <div class="row mb-3">
                                <div class="col-xl-3 col-sm-6">
                                    <div class="card bg-info text-white h-100">
                                        <div class="card-body bg-info">
                                            <div class="rotate">
                                                <i class="fa fa-home fa-3x"></i>    
                                            </div>
                                            <h6 class="text-uppercase">Available</h6>
                                            <h1 class="display-4" id='h1avail'></h1>
                                        </div>

                                        <div id="disable_pl" class='available_buttons'>
                                            <button type="button" class="btn btn-primary" data-val="20">Disable 20'</button>
                                            <button type="button" class="btn btn-primary" data-val="30">Disable 30' Rear Lane</button>
                                            <button type="button" class="btn btn-primary" data-val="31">Disable 30'</button>
                                            <button type="button" class="btn btn-primary" data-val="40">Disable 40'</button>
                                            <button type="button" class="btn btn-primary" data-val="45">Disable 45'</button>
                                            <!-- <button type="button" class="btn btn-primary" data-val="60">Disable 60'</button> -->
                                        </div>
                                    </div>


                                </div>
                                <div class="col-xl-3 col-sm-6">
                                    <div class="card text-white bg-danger h-100">
                                        <div class="card-body bg-danger">
                                            <div class="rotate">
                                                <i class="fa fa-usd fa-3x"></i>
                                            </div>
                                            <h6 class="text-uppercase">Sold</h6>
                                            <h1 class="display-4" id='h1sold'></h1>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-sm-6">
                                    <div class="card text-white bg-zqrning h-100">
                                        <div class="card-body bg-warning">
                                            <div class="rotate">
                                                <i class="fa fa-hourglass fa-3x"></i>
                                            </div>
                                            <h6 class="text-uppercase">Pending</h6>
                                            <h1 class="display-4" id='h1pending'></h1>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-sm-6">
                                    <div class="card text-white bg-success h-100">
                                        <div class="card-body bg-success">
                                            <div class="rotate">
                                                <i class="fa fa-ban fa-3x"></i>
                                            </div>
                                            <h6 class="text-uppercase">Unavailable</h6>
                                            <h1 class="display-4" id='h1unavail'></h1>
                                        </div>

                                        <div id="enable_pl" class='available_buttons'>
                                            <button type="button" class="btn btn-primary" data-val="20">Enable 20'</button>
                                            <button type="button" class="btn btn-primary" data-val="30">Enable 30'</button>
                                            <button type="button" class="btn btn-primary" data-val="31">Disable 31'</button>
                                            <button type="button" class="btn btn-primary" data-val="40">Enable 40'</button>
                                            <button type="button" class="btn btn-primary" data-val="45">Enable 45'</button>
                                            <!-- <button type="button" class="btn btn-primary" data-val="60">Enable 60'</button> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/row-->
                            <hr>

                            <h3>Table of all lots</h3>
                            <table id="db_admin_table_lots" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead class="thead-inverse"></thead>
                                <tbody></tbody>
                            </table>
                        </div>

                        <div id="db_tab_content_models" class="tab-pane fade" role="tabpanel">
                            <h3>Table of all models</h3>
                            <table id="db_admin_table_models" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead class="thead-inverse"></thead>
                                <tbody></tbody>
                            </table>
                        </div>

                        <div id="db_tab_content_ac" class="tab-pane fade" role="tabpanel">
                            <h2 class="sub-header mt-5 col-lg-12">
                                Architectual controls
                                <button id="btn_enable_display">Show Active display</button>
                                <button id="btn_enable_layout">Show Sold Layout</button>
                            </h2>

                            <div class="container_siteplan">
                                <div id="container_siteplan_svg"></div>
                                <div id="containers_layout_controls">

                                    <div class="input-group">
                                        <span class="input-group-addon">Rotation</span>
                                        <input type="number"id="lot_textrotation"  class="form-control">
                                        <span class="input-group-addon" id="btn_save_rotation">Save</span>
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon">Dot Scale</span>
                                        <input type="number"id="lot_scale"  class="form-control">
                                        <span class="input-group-addon" id="btn_save_scale">Save</span>
                                    </div>
                                </div>
                            </div>                     

                            <hr>
                            <div class='row'>                        
                                <h2 class="sub-header mt-5 col-lg-12">Siting</h2> 

                                <div class="col-lg-6">                            
                                    <div class="card mb-3">
                                        <div class="card-header">
                                            Allowed models
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title">Click on a model to block it</h4>                                    
                                            <div class="card-text" id='panel_rules_custom_allowed'></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="card mb-3">
                                        <div class="card-header">
                                            Blocked models
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title">Click on a model to allow it</h4>                                    
                                            <div class="card-text" id='panel_rules_custom_blocked'></div>
                                        </div>
                                    </div>
                                </div>

                                <hr>       
                                <h2 class="sub-header mt-3 col-lg-12">AC rules</h2> 
                                <div class="col-lg-12">
                                    <div class="card mb-3">
                                        <div class="card-header">
                                            Individual rules
                                        </div>

                                        <div class="card-body">
                                            <h4 class="card-title">Click to remove block from rule</h4>                                    
                                            <div class="card-text" id='panel_rules_details'></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="card mb-3">
                                        <div class="card-header">
                                            Street rules
                                        </div>

                                        <div class="card-body">
                                            <h4 class="card-title">Click to remove or add to street</h4>                                    
                                            <div class="card-text"  id='panel_streets_details'></div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div id="db_tab_content_siting" class="tab-pane fade" role="tabpanel">

                            <h1 class="display-4 d-none d-sm-block">
                                Table siting analysis
                            </h1>

                            <ul id="tabsJustified" class="nav nav-tabs nav-justified">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#" data-target="#tab20" data-toggle="tab">20' designs</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-target="#tab30" data-toggle="tab">30' designs</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-target="#tab31" data-toggle="tab">31' designs</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-target="#tab40" data-toggle="tab">40' designs</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-target="#tab45" data-toggle="tab">45' designs</a>
                                </li>                                
                            </ul>

                            <!--/tabs-->
                            <br>
                            <div class="tab-content">

                                <div class="tab-pane fade show active" id="tab20">
                                    <h3>20' design table</h3>                    
                                    <table id="db_admin_table_siting_20" class="table table-sm table-responsive table-striped table-bordered table-hover" cellspacing="0" width="100%">
                                        <thead>
                                            <tr class="thead_models"><th>Lot</th></tr>
                                            <tr class="thead_elevs"><th></th></tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                    <hr>
                                </div>



                                <div class="tab-pane fade" id="tab30">
                                    <h3>30' design table</h3>                    
                                    <table id="db_admin_table_siting_30" class="table table-sm table-responsive table-striped table-bordered table-hover" cellspacing="0" width="100%">
                                        <thead>
                                            <tr class="thead_models"><th>Lot</th></tr>
                                            <tr class="thead_elevs"><th></th></tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>

                                <div class="tab-pane fade" id="tab31">
                                    <h3>31' design table</h3>                    
                                    <table id="db_admin_table_siting_31" class="table table-sm table-responsive table-striped table-bordered table-hover" cellspacing="0" width="100%">
                                        <thead>
                                            <tr class="thead_models"><th>Lot</th></tr>
                                            <tr class="thead_elevs"><th></th></tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>

                                <div class="tab-pane fade" id="tab40">
                                    <h3>40' design table</h3>                    
                                    <table id="db_admin_table_siting_40" class="table table-sm table-responsive table-striped table-bordered table-hover" cellspacing="0" width="100%">
                                        <thead>
                                            <tr class="thead_models"><th>Lot</th></tr>
                                            <tr class="thead_elevs"><th></th></tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>

                                <div class="tab-pane fade" id="tab45">
                                    <h3>45' design table</h3>                    
                                    <table id="db_admin_table_siting_45" class="table table-sm table-responsive table-striped table-bordered table-hover" cellspacing="0" width="100%">
                                        <thead>
                                            <tr class="thead_models"><th>Lot</th></tr>
                                            <tr class="thead_elevs"><th></th></tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>                                
                            </div>   
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal_lots" class="modal fade">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit lot </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label  class="col-2 col-form-label">Number</label>
                            <div class="col-10">
                                <input class="form-control" type="text" value="" id="lot_number" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label  class="col-2 col-form-label">Product line</label>
                            <div class="col-10">
                                <select class="form-control" id="lot_size">
                                    <option value="20">20</option>
                                    <option value="30">30 Rear Lane</option>
                                    <option value="31">30</option>
                                    <option value="40">40</option>
                                    <option value="45">45</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label  class="col-2 col-form-label">Premiums</label>
                            <div class="col-10">
                                <input class="form-control" type="number" value="" id="lot_premiums">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label  class="col-2 col-form-label">Grading</label>
                            <div class="col-10">
                                <input class="form-control" type="text" value="" id="lot_grading">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label  class="col-2 col-form-label">Closing Date</label>
                            <div class="col-10">
                                <input class="form-control" type="text" value="" id="lot_closingdate">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label  class="col-2 col-form-label">Status</label>
                            <div class="col-10">
                                <select class="form-control" id="lot_status">
                                    <option value="1">Available</option>
                                    <option value="2">Unavailable</option>
                                    <option value="3">Pending</option>
                                    <option value="4">Sold</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label  class="col-2 col-form-label">Model</label>
                            <div class="col-10">
                                <select class="form-control" id="lot_model">

                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label  class="col-2 col-form-label">Client</label>
                            <div class="col-10">
                                <input class="form-control" type="text" value="" id="lot_client">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn_save_lot">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal_models" class="modal fade">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit model</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="form-group row">
                            <label  class="col-2 col-form-label">Product line</label>
                            <div class="col-10">
                                <input class="form-control" type="text" value="" id="model_pl" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label  class="col-2 col-form-label">Arch. code</label>
                            <div class="col-10">
                                <input class="form-control" type="text" value="" id="model_code" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label  class="col-2 col-form-label">Model</label>
                            <div class="col-10">
                                <input class="form-control" type="text" value="" id="model_name" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label  class="col-2 col-form-label">Elevation</label>
                            <div class="col-10">
                                <input class="form-control" type="text" value="" id="model_elevation">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label  class="col-2 col-form-label">Square feet</label>
                            <div class="col-10">
                                <input class="form-control" type="text" value="" id="model_size">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label  class="col-2 col-form-label">Price</label>
                            <div class="col-10">
                                <input class="form-control" type="number" value="" id="model_price">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label  class="col-2 col-form-label">Bedrooms</label>
                            <div class="col-10">
                                <input class="form-control" type="number" value="" id="model_bedrooms">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label  class="col-2 col-form-label">Bathrooms</label>
                            <div class="col-10">
                                <input class="form-control" type="number" value="" id="model_bathrooms">
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn_save_model">Save changes</button>
                    </div>
                </div>
            </div>
        </div>  

        <div id="modal_confirm" class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Confirm</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h2 id="confirm_text"></h2>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="save_confirm">Save changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <script src="lib/firebase/firebase.js"></script>
        <script src="lib/firebase/firebase-app.js"></script>
        <script src="lib/firebase/firebase-auth.js"></script>
        <script src="lib/firebase/firebase-database.js"></script>
        <script src="lib/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>

        <script src="lib/d3/d3.min.js" type="text/javascript"></script>
        <script src="lib/popper/popper.min.js"></script>
        <script src="lib/bootstrap-4.0.0-beta/js/bootstrap.min.js"></script>

        <script src="lib/datatables/jquery.dataTables.min.js"></script>
        <script src="lib/datatables/dataTables.bootstrap4.min.js"></script>
        <script src="lib/datatables/dataTables.select.min.js"></script>
        <script src="lib/datatables/dataTables.responsive.min.js"></script> 

        <script src="lib/datepicker/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="js/FunctionsSitePlan.js" type="text/javascript"></script>
        <script src="js/d3Functions.js" type="text/javascript"></script>

        <script src="js/Classes.js" type="text/javascript"></script>
        <script src="js/AdminDashboard.js" type="text/javascript"></script>

    </body>
</html>